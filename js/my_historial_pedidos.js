var search_pedidosp = '';
$(document).on('pageinit', '#historial', function () {
    //comprobar si existe tabla clientes
    app.comprobarBD();
    console.log("init historial");

    $("#autocomplete-input-historial").keyup(function () {
        search_pedidosp = $(this).val();
        var tamanio = 0;
        tamanio = search_pedidosp.length;
        if (tamanio > 2) {
            app.cargarPaginaPedidosHistorial();
        }
    });

});
var dataHistorial = {
    id : null,
    codigo : null,
};

$(document).on('vclick', '#historial-lista li a', function(){  
    dataHistorial.id = $(this).attr('data-id');
    dataHistorial.codigo = $(this).attr('data-codigo');
    
    $.mobile.changePage( "#detalle_historial", { transition: "slide", changeHash: true });
});


 app.cargarPaginaPedidosHistorial = function(){
    console.log('metodo cargar historial');
    app.db.transaction(listarHistorial,app.onError);
 }

function listarHistorial(tx){
    console.log('metodo cargar  listarHistorial de table');
    tx.executeSql("Select * from CabeceraPedidoServer where CodCliente like '%" + search_pedidosp + "%'",[],function(tx,result){
        var ncli=result.rows.length;
        if(ncli>0){
            $('#historial-lista').empty();
            for(var i=0; i<ncli; i++){
                var row = result.rows.item(i);   
                $('#historial-lista').append('<li data-filtertext="'+row['Descripcion']+'" ><a href="" data-codigo="'+row['Codigo'] +'" data-categoria="'+row['Categoria'] +'" data-id="' + row['id'] + '" data-desc="'+row['Descripcion']+'" data-stock="'+row['Stock']+'"><img src="img_ardiles/001.jpg"><p><b>' + row['Descripcion'] + '</p><p>Stock: ' + row['Stock'] + '</b></p></a></li>');
            }
            $('#historial-lista').listview('refresh');
        }else{
            console.log("no hay datos");
        }
    });
};

$(document).on('pagebeforeshow', '#detalle_producto', function(){  
    app.db.transaction(listarDetalleProductos,app.onError);   
    
});

function listarDetalleProductos(tx){
    console.log('metodo cargar detalle productos de table');
    console.log(dataProd.codigo);
    tx.executeSql("Select * from producto where Codigo=?",[ dataProd.codigo ],function(tx,result){
        var ncli=result.rows.length;
        console.log(result.rows.item);
        if(ncli>0){
            $('#productos-data').empty();
            var html = '';
            for(var i=0; i<ncli; i++){
                var row = result.rows.item(i);
                $('#productos-data').append('<li><b>Código: </b>'+row['Codigo']+'</li>');
                $('#productos-data').append('<li><b>Descripcion: </b>'+row['Descripcion']+'</li>');
                $('#productos-data').append('<li><b>Presentación: </b>'+row['Presentacion']+'</li>');
                $('#productos-data').append('<li><b>Stock: </b>'+row['Stock']+'</li>');
                $('#productos-data').append('<li><b>Categoría : </b>'+row['Categoria']+'</li>');    
            }
            $('#productos-data').listview('refresh');
        }else{
            console.log("no hay datos productos detalle");
        }
    });
}
