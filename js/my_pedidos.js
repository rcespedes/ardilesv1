$(document).on('pagebeforeshow', '#pedidos', function () {
//comprobar si existe tabla clientes
    app.comprobarBD();
    app.cargarPaginaPedidos();
    $('#sync_pedidos').click(function () {
//Comprobar si existe conexion
        var conn = checkConnection();
        if (conn) {
            var userLogueado = getCurrentUser();
            app.db.transaction(function (tx) {
                tx.executeSql("Select * from CabeceraPedido where id_pedido_remoto='0' and CodVendedor=? ", [userLogueado], function (tx, result) {
                    var ncli = result.rows.length;
                    if (ncli > 0) {
                        var html = [];
                        for (var i = 0; i < ncli; i++) {
                            var row = result.rows.item(i);
                            var obj_pedido = {
                                pedido: {
                                    cabecera: {
                                        uuid: '',
                                        "CodVendedor": row['CodVendedor'],
                                        "CodCanalVenta": row['CodCanalVenta'],
                                        "CodCliente": row['CodCliente'],
                                        "CodTipoDoc": row['CodTipoDoc'],
                                        "CodFormaPago": row['CodFormaPago'],
                                        "CodMoneda": "SOL",
                                        "Letras": row['Letras'],
                                        "ObsLetras": row['ObsLetras'],
                                        "Vcto_letra": row['Vcto_letra'],
                                        "Vcto_letra_sig": row['Vcto_letra_sig'],
                                        "CodAgencia": row['CodAgencia'],
                                        "DirEntrega": row['DirEntrega'],
                                        "DirFacturacion": row['DirFacturacion'],
                                        "FechaEntrega": row['FechaEntrega'],
                                        "Observaciones": row['Observaciones'],
                                        "MntSubTotal": row['MntSubTotal'],
                                        "MntIGV": row['MntIGV'],
                                        "MntTotal": row['MntTotal'],
                                        "CodMotivoNoPedido": row['CodMotivoNoPedido'],
                                        "FechaInicio": row['FechaInicio'],
                                        "FechaFin": row['FechaFin'],
                                        "FechaDB": row['FechaDB'],
                                        "FlgMigracion": "T",
                                        "MntBruto": row['MntBruto'],
                                        "MntDscto": row['MntDscto'],
                                        "Latitud": row['Latitud'],
                                        "Longitud": row['Longitud'],
                                        "GuiaRemision": row['GuiaRemision']
                                    },
                                    detalle: [],
                                }
                            };
                            var id_pedido_tmp = row['IdPedido'];
                            console.log("ID PEDIDOOO: " + id_pedido_tmp);
                            var pedido_detalla_tmp = [];
                            var pedido_detalle = [];
                            tx.executeSql("Select * from DetallePedido where IdPedido = ?", [id_pedido_tmp], function (tx, result) {
                                var ndeta = result.rows.length;
                                pedido_detalle = [];
                                console.log(ndeta);
                                if (ndeta > 0) {
                                    for (var o = 0; o < ndeta; o++) {
                                        var row_d = result.rows.item(o);
                                        console.debug(result.rows.item(o));
                                        var row_pedido_detalle = {
                                            "CodCategoria": row_d['CodCategoria'],
                                            "CodProducto": row_d['CodProducto'],
                                            "CodListaPrecio": row_d['CodListaPrecio'],
                                            "Cantidad": row_d['Cantidad'],
                                            "D1": row_d['D1'],
                                            "DetalleD1": row_d['DetalleD1'],
                                            "D2": row_d['D2'],
                                            "DetalleD2": row_d['DetalleD2'],
                                            "D3": row_d['D3'],
                                            "DetalleD3": row_d['DetalleD3'],
                                            "ValorBruto": row_d['ValorBruto'],
                                            "ValorDscto": row_d['ValorDscto'],
                                            "SubTotal": row_d['SubTotal'],
                                            "ValorIGV": row_d['ValorIGV'],
                                            "ValorTotal": row_d['ValorTotal'],
                                            "Presentacion": row_d['Presentacion'],
                                            "DsctoSucesivo": row_d['DsctoSucesivo'],
                                            "Precio": row_d['Precio']
                                        };
                                        pedido_detalle.push(row_pedido_detalle);
                                    }
                                    pedido_detalla_tmp = pedido_detalle;
                                    console.debug(pedido_detalla_tmp);
                                }

                                obj_pedido.pedido.detalle = pedido_detalla_tmp;
                                jQuery.ajaxSetup({
                                    contentType: "application/json; charset=utf-8",
                                    async: false,
                                    cache: false
                                });
                                var id_syncronizado = 0;
                                jQuery.ajax({
                                    url: config.rest_url + '/ws_pedidos',
                                    data: JSON.stringify(obj_pedido),
                                    method: 'POST',
                                    type: 'json',
                                    success: function (xhr) {
                                        id_syncronizado = xhr.id;
                                        if (parseInt(xhr.id) > 0) {
                                            updateDetallePedido(id_syncronizado, id_pedido_tmp);
                                            $('#pedidos-lista').empty();
                                            $('#pedidos-lista').listview('refresh');
                                            if (produccion) {
                                                navigator.notification.alert(
                                                        'Se guardó el pedido de manera correcta.', // message
                                                        alertDismissed, // callback
                                                        '', // title
                                                        'Aceptar'                  // buttonName
                                                        );
                                            } else {
                                                alert('Se guardó el pedido de manera correcta.');
                                            }
                                            app.cargarPaginaPedidos();
                                        }
                                    }
                                });
                            });

                        }
                    } else {
                        console.log("no hay datos forma de pago");
                    }
                });
            });
        } else {
            if (produccion) {
                navigator.notification.alert(
                        'Debe conectarse a internet para poder realizar una sincronización.', // message
                        alertDismissed, // callback
                        '', // title
                        'Aceptar'                  // buttonName
                        );
            } else {
                alert('Debe conectarse a internet para poder realizar una sincronización.');
            }
        }

    });
});
app.cargarPaginaPedidos = function () {
    console.log('metodo cargar pedidos');
    app.db.transaction(listarPedidos, app.onError);
}
//var arrayPedidosHist = [];
var pl_cantidad = 0;
var pl_monto_total = 0;
function listarPedidos(tx) {
    pl_cantidad = 0;
    pl_monto_total = 0;
    
    tx.executeSql("Select c.IdPedido,c.CodCliente,c.CodMotivoNoPedido,c.id_pedido_remoto,c.MntTotal,cl.Descripcion from CabeceraPedido c, clientes cl where c.CodCliente=cl.Codigo and c.CodVendedor=?", [getCurrentUser()], function (tx, result) {
        var ncli = result.rows.length;
        pl_cantidad = ncli;

        if (ncli > 0) {
            $('#pedidos-lista').empty();

            for (var i = 0; i < ncli; i++) {
                var row = result.rows.item(i);
                var sync = 'Pendiente';
                var montoTotal_ = row['MntTotal'];
                var montoTotal = parseFloat(montoTotal_.toFixed(4));
                if (parseInt(row['id_pedido_remoto']) !== 0) {
                    sync = 'Sincronizado';
                }
                pl_monto_total += parseFloat(montoTotal);
                var nopedido = '';
                var nopedidohtml = '';
                if (row['CodMotivoNoPedido'] !== '000') {
                    if (parseInt() === 1) {
                        nopedido = 'CERRADO';
                    } else if (parseInt(row['CodMotivoNoPedido']) === 2) {
                        nopedido = 'COMPRADOR AUSENTE';
                    } else if (parseInt(row['CodMotivoNoPedido']) === 3) {
                        nopedido = 'TIENE STOCK';
                    } else if (parseInt(row['CodMotivoNoPedido']) === 4) {
                        nopedido = 'MOROSO';
                    } else if (parseInt(row['CodMotivoNoPedido']) === 5) {
                        nopedido = 'BAJA POR SUPERVISAR';
                    } else if (parseInt(row['CodMotivoNoPedido']) === 6) {
                        nopedido = 'NEGOCIACION';
                    } else if (parseInt(row['CodMotivoNoPedido']) === 7) {
                        nopedido = 'COBRANZA';
                    } else {
                        nopedido = '';
                    }

                    nopedidohtml = '<p>Motivo: ' + nopedido + '</p>';
                }
                
                
                if(row['CodMotivoNoPedido'] === '000'){
                    $('#pedidos-lista').append('<li data-filtertext="' + row['CodCliente'] + '" ><a href="" data-id="' + row['IdPedido'] + '"><p><b>' + row['Descripcion'] + '</p><p><b>Cod. cliente: ' + row['CodCliente'] + '</p><p>Monto Total: </b>' + montoTotal + ' / ' + sync + '</p>' + nopedidohtml + '</a></li>');
                }else{
                    $('#pedidos-lista').append('<li data-filtertext="' + row['CodCliente'] + '" ><b> ' + row['Descripcion'] + '</p><p><b>Cod. cliente: ' + row['CodCliente'] + '</p><p>Monto Total: </b>' + montoTotal + ' / ' + sync + '</p>' + nopedidohtml + '</li>');
                }
                
            }
            $('#pedidos-lista').listview('refresh');
        } else {
            console.log("no hay datos");
        }

        $('#pl_cantidad').val(pl_cantidad);
        $('#pl_total').val(pl_monto_total);
    });


}

var IdHistorial = {
    id: null
};
$(document).on('vclick', '#pedidos-lista li a', function () {
    IdHistorial.id = $(this).attr('data-id');
    console.log(IdHistorial);
    $.mobile.changePage("#detalle-historial-pedido", {transition: "slide", changeHash: true});
});

//historial-pedidos-lista

$(document).on('pagebeforeshow', '#detalle-historial-pedido', function () {
    app.db.transaction(app.listaPedidosHistorial, app.onError);
});

app.listaPedidosHistorial = function () {
    app.db.transaction(listarPedidosDetalle, app.onError);
}

function listarPedidosDetalle(tx) {
    tx.executeSql("select d.IdPedido,d.CodProducto,d.CodListaPrecio,d.Cantidad,d.Subtotal,p.Descripcion from DetallePedido d,Producto p where d.CodProducto=p.Codigo and d.IdPedido=?", [parseInt(IdHistorial.id)], function (tx, result) {
        var ncli = result.rows.length;
        console.log(result.rows);
        if (ncli > 0) {
            $('#historial-pedidos-lista').empty();
            arrayPedidosHist = [];
            for (var i = 0; i < ncli; i++) {
                var row = result.rows.item(i);
                $('#historial-pedidos-lista').append('<li><p><b>' + row['Descripcion'] + '</p><p><b>Cod. producto: ' + parseInt(row['CodProducto']) + '</p><p>Cantidad: </b>' + row['Cantidad'] + '</p></li>');
            }
            $('#historial-pedidos-lista').listview('refresh');
        } else {
            console.log("no hay datos");
        }
    });
}

