$(document).on('pagebeforeshow', '#clientes', function () {
//comprobar si existe tabla clientes
    //   jQuery("#frm_clientes_ini")[0].reset();
    app.comprobarBD();
    app.cargarPaginaClientes();
});

app.cargarPaginaClientes = function () {
    console.log('metodo cargar clientes');
    app.db.transaction(listarClientes, app.onError);
}

function listarClientes(tx) {
    console.log('metodo cargar  clientes de table');
    var userLogueado = getCurrentUser();
    tx.executeSql("Select * from Clientes where CodVendedor=? order by descripcion", [userLogueado], function (tx, result) {
        var ncli = result.rows.length;
        if (ncli > 0) {
            $('#clientes-lista').empty();
            for (var i = 0; i < ncli; i++) {
                var row = result.rows.item(i);
                $('#clientes-lista').append('\
                    <li data-filtertext="' + row['Descripcion'] + row['Ruc'] + '" >\n\
                    <a href="" data-id="' + row['Codigo'] + '" \n\
                            data-agencia="' + row['Agencia'] +
                        '" data-codagencia="' + row['CodAgencia'] +
                        '" data-desc="' + row['Descripcion'] + '">\n\
                        <h3>' + row['Descripcion'] + '</h3></a></li>');
            }
            $('#clientes-lista').listview('refresh');
        } else {
            console.log("no hay datos en clientes");
        }
    });
}

$(document).on('pagebeforeshow', '#detalle_cliente', function () {

    jQuery('#btn_nuevo_pedido').off('vclick');

    jQuery('#btn_nuevo_pedido').on('vclick', function () {
        
        jQuery('#form_pedido_detalle').trigger("reset");
        jQuery('#span_subtotal').html('0');
        jQuery('#span_igv').html('0');
        jQuery('#span_total').html('0');
        jQuery('#select-agencia').val('');
        jQuery('#select-agencia-des').html('');
        jQuery('#productos-pedido-lista').listview('refresh');
        jQuery('#productos-pedido-lista-detalle').empty();
//        activarPanel('cabecera');
        limpiarPanel('frm_cabecera_pedidos');
        
    });
    app.db.transaction(listarDetalleClientes, app.onError);
});
$(document).on('vclick', '#clientes-lista li a', function () {
    dataCliente.id = $(this).attr('data-id');
    dataCliente.desc = $(this).attr('data-desc');
    dataCliente.agencia = $(this).attr('data-agencia');
    dataCliente.codagencia = $(this).attr('data-codagencia');
    dataCliente.fechainicio = fechaInicial();
    $.mobile.changePage("#detalle_cliente", {transition: "slide", changeHash: true});

});
var dataCliente = {
    id: null,
    desc: null,
    agencia: null,
    codagencia: null,
}
function listarDetalleClientes(tx) {
    console.log('metodo cargar detalle clientes de table');
    tx.executeSql("Select * from Clientes where Codigo=?", [dataCliente.id], function (tx, result) {
        var ncli = result.rows.length;
        if (ncli > 0) {
            $('#cliente-data').empty();
            var html = '';
            for (var i = 0; i < ncli; i++) {
                var row = result.rows.item(i);
                $('#cliente-data').append('<li><b style="color:#0066CC;">Código: </b>' + row['Codigo'] + '</li>');
                $('#cliente-data').append('<li><b style="color:#0066CC;">Nombres: </b>' + row['Descripcion'] + '</li>');
                $('#cliente-data').append('<li><b style="color:#0066CC;">Tipo de negocio: </b>' + row['DescripcionNegocio'] + '</li>');
                $('#cliente-data').append('<li><b style="color:#0066CC;">Línea de crédito : </b>' + row['LimiteCredito'] + '</li>');
                $('#cliente-data').append('<li><b style="color:#0066CC;">Línea disponible : </b>' + row['LimiteCreditoDisp'] + '</li>');
                
            }
            $('#cliente-data').listview('refresh');
        } else {
            console.log("no hay datos clientes detalle");
        }
    });
}


