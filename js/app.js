$(document).bind('mobileinit', function () {
    $.mobile.pushStateEnabled = false;
});
var produccion = true;
var rndm = Math.random();

if (!produccion) {
    $(document).ready(function () {
        init_sistema();
    });
}

document.addEventListener("deviceready", init_sistema, false);

//Variables globales
var app = {};
app.db = null;
var arr_cliente = [];
var arr_producto = [];
var arr_pedido = [];
var session = false;
var session_usr = '';

function init_sistema() {
    //checkConnection();
    //inicia bd
    app.openDb();
    //comprobar login activo
    app.comprobarBDLogin();

    document.addEventListener("backbutton", function (e) {
        if ($.mobile.activePage.is('#inicio')) {
            e.preventDefault();
            // navigator.app.exitApp();
            logout();
        }
        else {
            navigator.app.backHistory();
//            window.history.back();
        }
    }, false);

//                console.log(session);
}
function init() {
    //crear tabla login
//    app.createTableLogin();
    //comprueba si existen tablas creadas
    app.comprobarBD();
}

function fechaInicial() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;//January is 0! 
    var yyyy = today.getFullYear();
    var hh = today.getHours();
    var mi = today.getMinutes();
    var ss = today.getSeconds();

    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    var fecha = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + mi + ':' + ss;
    return fecha;
}
app.openDb = function () {
    if (window.sqlitePlugin !== undefined) {
        //Llamando a bd con SQLITE
        console.log('SQLITE');
        app.db = window.sqlitePlugin.openDatabase("bd_sqlite_ardiles");
        app.createTableLogin();
        app.consumeLogin();
    } else {
        console.log('WEBSQL');
        //Llamando a bd con websql para pruebas
        app.db = window.openDatabase("ardiles_local", "1.0", "Cordova Demo", 200000);
        app.createTableLogin();
        app.consumeLogin();
    }
}
//Comprobar tabla login
app.comprobarBDLogin = function () {
    existeTbLogin('d');
//    app.db.transaction(existeTbLogin, app.onError);
}

function existeTbLogin(tx) {

    if (isLoged()) {
        $.mobile.changePage("#menupri");
    } else {
        $.mobile.changePage("#inicio");
    }
}

//Comprobar tablas sincronizadas
app.comprobarBD = function () {
    app.db.transaction(existeTbClientes, app.onError);
}

function existeTbClientes(tx) {
    tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='clientes'", [], function (tx, result) {
        if (result.rows.length == 0) {
//            if (produccion) {
//                navigator.notification.alert(
//                        'Debe realizar la sincronización del aplicativo.', // message
//                        alertDismissed, // callback
//                        '', // title
//                        'Aceptar'                  // buttonName
//                        );
//            } else {
//                alert('Debe realizar la sincronización del aplicativo.');
//            }
            return;
        }
    });
}
var splash_id;
$(document).on('pageinit', '#portada', function () {

    splash_id = setInterval(splash, 500);

});

function splash() {
    if (isLoged()) {
        $.mobile.changePage("#menupri");
    } else {
        $.mobile.changePage("#inicio");
    }
    clearInterval(splash_id);
}
$(document).on('pageinit', '#inicio', function () {
    logout();
});

$(document).on('pagebeforeshow', '#inicio', function () {
    //    listarClientes();
    // checkConnection();
});

//Cambio a pantalla sincronizar
$(document).on('pageinit', '#menupri', function () {

    jQuery('#btn_logout').on('vclick', function () {

        var r = confirm("¿Está seguro de cerrar su sesión?");
        if (r == true) {
            logout();
            cerrarApp();
        } else {
            return false;
        }


        return;
    });

    //Abro conexión a la bd interna
//    if (isLoged() !== true) {
//        $.mobile.changePage("#inicio");
//    }   
    init();
});


//Cambio a pantalla sincronizar
$(document).on('pagebeforeshow', '#menu_sincronizar', function () {
    $('#sync-lista-detalle').empty();
    app.cargarListaSincronizacion();
    $('#sync-lista-detalle').listview('refresh');
});

//Método cargar listado de tablas sincronizadas
app.cargarListaSincronizacion = function () {
    console.log('metodo cargar sincronizacion');
    app.db.transaction(listarSincronizacion, app.onError);
}
//lista de tablas sincronizadas
function listarSincronizacion(tx) {
    console.log('metodo cargar  sincronizacion de table');
    var user = getCurrentUser();
    if (user === false) {
        if (produccion) {
            navigator.notification.alert(
                    'No existe usuario logueado.', // message
                    alertDismissed, // callback
                    '', // title
                    'Aceptar'                  // buttonName
                    );
        } else {
            alert('No existe usuario logueado.');
        }


        return;
    }
    tx.executeSql("Select * from log_sync where CodVendedor=?", [user], function (tx, result) {
        var ncli = result.rows.length;
        if (ncli > 0) {
            $('#sync-lista-detalle').empty();
            for (var i = 0; i < ncli; i++) {
                var row = result.rows.item(i);
                $('#sync-lista-detalle').append('<li><a href=""><h3>' + row['tipo'] + '</h3><p><strong>' + row['fecha_sync'] + '</strong></p></a></li>');
            }
            $('#sync-lista-detalle').listview('refresh');
        } else {
            $('#sync-lista-detalle').append('<li><a href=""><h3>No existen datos sincronizados.</h3></a></li>');
            $('#sync-lista-detalle').listview('refresh');
            console.log("no hay datos");
        }
    });
}

function confirmaLogout(btn) {
    alert(btn);
    return false;
//    if (btn === 1) {
//        logout();
//    } else {
//        return false;
//    }
}


jQuery(document).ready(function () {

    $(".logo").animate({'top': '20px'}, 'slow', "easeInOutCirc");
    $(".cartitems").delay(1000).animate({'width': '30px', 'height': '30px', 'top': '10px', 'right': '10px', 'opacity': 1}, 1000, "easeOutBounce");
    $(".main-nav ul > li")
            .css('opacity', '0')
            .each(function (index, item) {
                setTimeout(function () {
                    $(item).fadeTo('slow', 1, "easeInOutCirc");
                }, index * 175);
            });

    $(".main-nav ul > li span")
            .css('opacity', '0')
            .each(function (index, item) {
                setTimeout(function () {
                    $(item).animate({'left': '0px', 'opacity': 1}, 500, "easeInOutCirc");
                }, index * 175);
            });

});