$(document).on('pagebeforeshow', '#popupProductos', function () {
    $('#autocomplete-input-pedido').val('');
    $('#productos-pedido-lista').empty();
    //Cambia caja de texto cantidad

    $('#frm_cantidad').on('input', function (e) {
//        if ($('#frm_cantidad').val() !== '') {
//            calcula_precio_1("#frm_cantidad");
//        }
        calcula_precio_1("#frm_cantidad");
    });

    //Cambia Lista de precios    
    $("#select-precios").change(function () {
        calcula_precio_1("#select-precios");
    });

    //Cambia lista de descuento 1
    $("#select-desc1").change(function () {
        calcula_precio_1("#select-desc1");
    });

    //Cambia lista de descuento 2
    $("#select-desc2").change(function () {
        calcula_precio_1("#select-desc2");
    });

    //Cambia lista de descuento 3
    $("#select-desc3").change(function (event) {
        calcula_precio_1("#select-desc3");
    });
});

function calcula_precio_1(select) {

    if (jQuery('#select-precios').val() === '') {
        return;
    }
    if (jQuery('#select-precios > option:checked').data('precio') === undefined) {
        jQuery(select).val(0);
        return;
    }

    var cantidad = $('#frm_cantidad').val();
    if (cantidad === '' || cantidad.length === 0) {
//        jQuery(select).val(0);
        cantidad = 0;
//        alert('Debe ingresar una cantidad');
        return;
    }


    var valor = calcular_precio_final(
            parseFloat(jQuery('#select-precios > option:checked').data('precio')),
            parseFloat(cantidad),
            parseFloat(jQuery("#select-desc1").val()),
            parseFloat(jQuery("#select-desc2").val()),
            parseFloat(jQuery("#select-desc3").val()))
            ;

    jQuery('#frm_total').html('<b>Valor Total</b>:' + numberWithCommas(valor.valorTotal.toFixed(2)));
    jQuery('#frm_igv').html('<b>I.G.V:</b> ' + numberWithCommas(parseFloat(valor.ValorIGV).toFixed(2)));

    jQuery('#frm_valor_bruto').html('<b>Valor Bruto:</b> ' + numberWithCommas(valor.ValorBruto.toFixed(2)));
    jQuery('#frm_valor_descuento').html('<b>Descuento:</b> ' + numberWithCommas(parseFloat(valor.ValorDscto).toFixed(2)));
    jQuery('#frm_sub_total').html('<b>Sub Total:</b> ' + numberWithCommas(valor.ValorSubTotal.toFixed(2)));
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

$(document).on('pagebeforeshow', '#detalle_cliente', function () {
    app.db.transaction(listarFormaPago, app.onError);
    app.db.transaction(listarDetalleDireccionClientes, app.onError);
    app.db.transaction(listarBancos, app.onError);
    $("#cabecera_nombre").val(dataCliente.desc);
    $("#cabecera_codigo").val(dataCliente.id);
    function listarFormaPago(tx) {
        tx.executeSql("Select * from FormaPago where FlgRegHabilitado='T' ", [], function (tx, result) {
            var ncli = result.rows.length;
            if (ncli > 0) {
                $('#select-forma-pago').empty();
                var html = [];
                html.push('<option>--Seleccione--</option>');
                for (var i = 0; i < ncli; i++) {
                    var row = result.rows.item(i);
                    html.push('<option value="' + row['Codigo'] + '">' + row['Descripcion'] + '</option>');
                }
                $('#select-forma-pago').html(html.join(''));
            } else {
                console.log("no hay datos forma de pago");
            }
        });
    }


    function listarDetalleDireccionClientes(tx) {
        tx.executeSql("Select * from Direccioncliente where Codigo=?", [dataCliente.id], function (tx, result) {
            var ncli = result.rows.length;
            //console.log(result.rows); 
            if (ncli > 0) {
                $('#select-direccion').empty();
                $('#select-direccion-ennvio').empty();
                var html = [];
                html.push('<option>--Seleccione--</option>');
                for (var i = 0; i < ncli; i++) {
                    var row = result.rows.item(i);
                    html.push('<option value="' + row['Codigo'] + '">' + row['Descripcion'] + '</option>');
                }
                $('#select-direccion').html(html.join(''));
                $('#select-direccion-ennvio').html(html.join(''));
            } else {
                console.log("no hay datos en lista direccion ");
            }
        });
    }

    function listarBancos(tx) {
        tx.executeSql("Select Codigo,NombreCorto  from Bancos where FlgRegHabilitado='T' ", [], function (tx, result) {
            var ncli = result.rows.length;
            //console.log(result.rows); 
            if (ncli > 0) {
                $('#select-bancos').empty();
                var html = [];
                html.push('<option>--Seleccione--</option>');
                for (var i = 0; i < ncli; i++) {
                    var row = result.rows.item(i);
                    html.push('<option value="' + row['Codigo'] + '">' + row['NombreCorto'] + '</option>');
                }
                $('#select-bancos').html(html.join(''));
            } else {
                console.log("no hay datos en lista direccion ");
            }
        });
    }

});


var search_producto = '';

$("#autocomplete-input-pedido").keyup(function () {
    search_producto = $(this).val();
    var tamanio = 0;
    tamanio = search_producto.length;
    if (tamanio > 2) {
        app.cargarPaginaDetallePedidosProductos();
    }
});
var search_agencias = '';

$("#autocomplete-input-agencias").keyup(function () {
    search_agencias = $(this).val();
    var tamanio = 0;
    tamanio = search_agencias.length;
    if (tamanio > 2) {
        app.cargarAgencias();
    }
});


app.cargarPaginaDetallePedidosProductos = function () {
    console.log('entre a detalle productos pedidos');
    app.db.transaction(listarProductosPedidos, app.onError);
}

function listarProductosPedidos(tx) {
    tx.executeSql("Select Categoria,Codigo,id,Descripcion,Stock,CodCategoria,Presentacion from producto where Descripcion like '%" + search_producto + "%' OR Codigo  like '" + search_producto + "%' ", [], function (tx, result) {
        var ncli = result.rows.length;
        if (ncli > 0) {
            $('#productos-pedido-lista').empty();
            for (var i = 0; i < ncli; i++) {
                var row = result.rows.item(i);
                var stock = row['Stock'];
                $('#productos-pedido-lista').append('<li data-filtertext="' + row['Descripcion'] + ' ' + row['Codigo'] + '"  data-icon="plus"><a href="#popupCantidad" data-unidad="' + row['Presentacion'] + '" data-desccategoria="' + row['Categoria'] + '" data-stock="' + stock + '" data-categoria="' + row['CodCategoria'] + '" data-rel="dialog" data-codigo="' + row['Codigo'] + '" data-desc="' + row['Descripcion'] + '" data-id="' + row['id'] + '"><p class="font-14">CODIGO: ' + row['Codigo'] + ' / UNIDAD: ' + row['Presentacion'] + '</p><p class="font-16"><b>' + row['Descripcion'] + '</p><p class="font-16">STOCK : ' + stock + '</b></p></li>');
            }
            $('#productos-pedido-lista').listview('refresh');
        } else {
            console.log("no hay datos");
        }
    });
}

app.cargarAgencias = function () {
    console.log('entre a listar agencias');
    app.db.transaction(listarAgencias, app.onError);
}

function listarAgencias(tx) {
    console.log('listar entre a detalle productos pedidos');
    tx.executeSql("select id,Codigo,Descripcion,FlgRegHabilitado from Agencias where FlgRegHabilitado='T' and Descripcion like '" + search_agencias + "%'", [], function (tx, result) {
        var ncli = result.rows.length;
        if (ncli > 0) {
            $('#listado_agencias').empty();
            for (var i = 0; i < ncli; i++) {
                var row = result.rows.item(i);
                $('#listado_agencias').append('<li data-filtertext="' + row['Descripcion'] + '"  data-icon="plus"><a href="#" data-rel="dialog" data-codigo="' + row['Codigo'] + '" data-desc="' + row['Descripcion'] + '" data-id="' + row['id'] + '"><p><b>' + row['Descripcion'] + '</p></li>');
            }
            $('#listado_agencias').listview('refresh');
        } else {
            console.log("no hay datos");
        }
    });
}

$(document).on('vclick', '#productos-pedido-lista li a', function () {

    dataProductoDetalle.id = $(this).attr('data-id');
    dataProductoDetalle.codigo = $(this).attr('data-codigo');
    dataProductoDetalle.desc = $(this).attr('data-desc');
    dataProductoDetalle.categoria = $(this).attr('data-categoria');
    dataProductoDetalle.desccategoria = $(this).attr('data-desccategoria');
//    dataProductoDetalle.precio = $(this).attr('data-precio');
//    dataProductoDetalle.codigoprecio = $(this).attr('data-codigolprecio');
    dataProductoDetalle.unidad = $(this).attr('data-unidad');
    dataProductoDetalle.stock = $(this).attr('data-stock');
    console.log("entre al detalle producto pedido lista");

});
$(document).on('vclick', '#listado_agencias li a', function () {

    $('#select-agencia').val($(this).attr('data-codigo'));
    $('#select-agencia-des').html($(this).attr('data-desc'));
    $.mobile.changePage('#page_cabecera_pedido', {transition: "flip"});

});


function eliminar_producto_detalle(precio, cantidad, frm_d1, frm_d2, frm_d3) {
    actualizar_totales(-1, precio, cantidad, frm_d1, frm_d2, frm_d3);
}
function eliminar_producto_detalle2(id, precio, cantidad, frm_d1, frm_d2, frm_d3) {
    actualizar_totales(-1, precio, cantidad, frm_d1, frm_d2, frm_d3);
    jQuery(id).parent().remove();
}

function actualizar_lista_precios() {
    var ValorPrecio, ValorCantidad, ValorDscto1, ValorDscto2, ValorDscto3;
    var p_cantidad = $('#frm_cantidad').val();
    var p_descuento1 = $('#select-desc1').val();
    var p_descuento2 = $('#select-desc2').val();
    var p_descuento3 = $('#select-desc3').val();
    var p_precio = $('#select-precios option:checked').data('precio');

    ValorPrecio = parseFloat(p_precio);
    ValorCantidad = parseFloat(p_cantidad);

    ValorDscto1 = parseFloat(p_descuento1) / 100;
    ValorDscto2 = parseFloat(p_descuento2) / 100;
    ValorDscto3 = parseFloat(p_descuento3) / 100;
//
//    console.log(ValorPrecio);
//    console.log(ValorCantidad);
//    console.log(ValorDscto1);
//    console.log(ValorDscto2);
//    console.log(ValorDscto3);
}

function strip_(number) {
    return parseFloat(parseFloat(number).toPrecision(12));
}

function actualizar_totales(op, p_precio, p_cantidad, p_descuento1, p_descuento2, p_descuento3) {

//    console.debug('OP ' + op);
//    console.debug('p_precio ' + p_precio);
//    console.debug('p_cantidad ' + p_cantidad);
//    console.debug('p_descuento1 ' + p_descuento1);
//    console.debug('p_descuento2 ' + p_descuento2);
//    console.debug('p_descuento3 ' + p_descuento3);


    var sub_total_span = parseFloat(jQuery('#span_subtotal').text());
    var ValorCantidad, ValorBruto, ValorDscto, ValorSubTotal, ValorIGV,
            ValorTotal, ValorDscto1, ValorDscto2, ValorDscto3, ValorDetalle1,
            ValorDetalle2, ValorDetalle3, ValorDsctoSucesivo,
            ValorPrecio;

    ValorDsctoSucesivo = 0;
    ValorPrecio = parseFloat(p_precio);
    ValorCantidad = parseFloat(p_cantidad);
    ValorDscto1 = (100 - parseFloat(p_descuento1)) / 100;
    ValorDscto2 = (100 - parseFloat(p_descuento2)) / 100;
    ValorDscto3 = (100 - parseFloat(p_descuento3)) / 100;




    //Valor bruto
    ValorBruto = strip_(ValorPrecio * ValorCantidad);
    ValorDetalle1 = strip_(ValorDscto1 * ValorBruto);
    ValorDetalle2 = strip_(ValorDscto2 * ValorDetalle1);
    ValorDetalle3 = strip_(ValorDscto3 * ValorDetalle2);

    //ValorDscto = ValorDetalle1 + ValorDetalle2 + ValorDetalle3 + ValorDsctoSucesivo;
    // ValorDscto = ValorDetalle3;
    //ValorSubTotal = ValorBruto - ValorDscto;
    ValorSubTotal = ValorDetalle3;
    ValorIGV = strip_(ValorSubTotal * 0.18);
    //Valor total
    ValorTotal = ValorSubTotal + ValorIGV;
    if (op === 1) {
        sub_total_span = sub_total_span + ValorSubTotal;
    }
    if (op === -1) {
        sub_total_span = sub_total_span - ValorSubTotal;
    }

    if (sub_total_span <= 0) {
        sub_total_span = 0;
    }
//    jQuery('#span_subtotal').text(Math.ceil(sub_total_span).toFixed(2));

//    jQuery('#span_subtotal').text(sub_total_span);

    var igv = sub_total_span * 0.18;
    var total = sub_total_span + igv;

//    console.debug('IGV: ' + igv);
//    console.debug('TOTAL: ' + total);
//    jQuery('#span_igv').text(Math.ceil(igv).toFixed(2));
//    jQuery('#span_total').text(Math.ceil(total).toFixed(2));
    jQuery('#span_subtotal').html(numberWithCommas(sub_total_span.toFixed(2)));
    jQuery('#span_igv').html(numberWithCommas(igv.toFixed(2)));
    jQuery('#span_total').html(numberWithCommas(total.toFixed(2)));

//    jQuery('#span_subtotal_resumen').html(sub_total_span.toFixed(4));
//    jQuery('#span_igv_resumen').html(igv.toFixed(4));
//    jQuery('#span_total_resumen').html(total.toFixed(4));
//    jQuery('#span_igv').text(igv);
//
//    jQuery('#span_total').text(total);

}

function calcular_precio_final(p_precio, p_cantidad, p_descuento1, p_descuento2, p_descuento3) {

//    var sub_total_span = parseFloat(jQuery('#span_subtotal').text());
    var ValorCantidad, ValorBruto, ValorDscto, ValorSubTotal, ValorIGV,
            ValorTotal, ValorDscto1, ValorDscto2, ValorDscto3, ValorDetalle1,
            ValorDetalle2, ValorDetalle3, ValorDsctoSucesivo,
            ValorPrecio;

    ValorDsctoSucesivo = 0;
    ValorPrecio = parseFloat(p_precio);
    ValorCantidad = parseFloat(p_cantidad);
    ValorDscto1 = ((100 - parseFloat(p_descuento1)) / 100);
    ValorDscto2 = ((100 - parseFloat(p_descuento2)) / 100);
    ValorDscto3 = ((100 - parseFloat(p_descuento3)) / 100);
    //Valor bruto
    ValorBruto = strip_(ValorPrecio * ValorCantidad); //
    ValorDetalle1 = ValorDscto1 * ValorBruto;
    ValorDetalle2 = ValorDscto2 * ValorDetalle1;
    ValorDetalle3 = ValorDscto3 * ValorDetalle2;
    //ValorDscto = ValorDetalle1 + ValorDetalle2 + ValorDetalle3 + ValorDsctoSucesivo;
    //ValorSubTotal = ValorBruto - ValorDscto;
    ValorDscto = ValorBruto - ValorDetalle3;
    ValorSubTotal = ValorDetalle3;
    ValorIGV = strip_(ValorSubTotal * 0.18);        //
    //Valor total
    ValorTotal = ValorSubTotal + ValorIGV;


    var obj = {
        valorTotal: ValorTotal,
        ValorBruto: ValorBruto,
        ValorIGV: ValorIGV.toFixed(4),
        ValorSubTotal: ValorSubTotal,
        ValorDscto: ValorDscto.toFixed(4),
    };
    return obj;
}
var dataProductoDetalle = {
    id: null,
    codigo: null,
    desc: null,
    categoria: null,
    precio: null,
    unidad: null,
    desccategoria: null,
    stock: null,
}

$(document).on('pageshow', '#popupCantidad', function () {

    jQuery("#frm_popupCantidad")[0].reset();

    if (producto_tmp !== null) {
        jQuery('#frm_precio').val(producto_tmp.precio);
        jQuery('#frm_cantidad').val(producto_tmp.cantidad);
        jQuery('#select-desc1').val(producto_tmp.dscto1);
        jQuery('#select-desc2').val(producto_tmp.dscto2);
        jQuery('#select-desc3').val(producto_tmp.dscto3);
    }

    $('#tx_nombre_producto').html('<b>(' + dataProductoDetalle.codigo + ') </b>' + dataProductoDetalle.desc + '');
    var stock =
            $('#frm_stock').html('<b>Stock:&nbsp;&nbsp;&nbsp;&nbsp;</b>' + parseFloat(dataProductoDetalle.stock).toFixed(0) + ' ' + dataProductoDetalle.unidad);



});
$(document).on('pagebeforeshow', '#popupCantidad', function () {
    jQuery("#frm_popupCantidad")[0].reset();
    app.db.transaction(listarDscto1, app.onError);
    app.db.transaction(listarDscto2, app.onError);
    app.db.transaction(listarDscto3, app.onError);
    app.db.transaction(listarListaPrecios, app.onError);


});


function listarDscto1(tx) {
    var categoria = $.trim(dataProductoDetalle.categoria);
    tx.executeSql("Select * from Descuentos where Tipo='D1' and Lista=? and Valor!=0", [parseInt(categoria)], function (tx, result) {
        var ncli = result.rows.length;
        var html = [];
        html.push('<option value="0" data-min="0" data-max="0">0%</option>');
        if (ncli > 0) {
            $('#select-desc1').empty();
            for (var i = 0; i < ncli; i++) {
                var row = result.rows.item(i);
                html.push('<option value="' + row['Valor'] + '" data-min="' + row['Min_m'] + '" data-max="' + row['Max_m'] + '">' + row['Valor'] + '%</option>');
            }
        } else {
            console.log("no hay datos forma de pago");
        }
        $('#select-desc1').html(html.join(''));
    });
}
function listarDscto2(tx) {
    var categoria = $.trim(dataProductoDetalle.categoria);
    tx.executeSql("Select * from Descuentos where Tipo='D2' and Lista=? and Valor!=0", [parseInt(categoria)], function (tx, result) {
        var ncli = result.rows.length;
        var html = [];
        html.push('<option value="0" data-min="0" data-max="0">0%</option>');
        if (ncli > 0) {
            $('#select-desc2').empty();
            for (var i = 0; i < ncli; i++) {
                var row = result.rows.item(i);
                html.push('<option value="' + row['Valor'] + '" data-min="' + row['Min_m'] + '" data-max="' + row['Max_m'] + '">' + row['Valor'] + '%</option>');
            }
        } else {
            console.log("no hay datos forma de pago");
        }
        $('#select-desc2').html(html.join(''));
    });
}
function listarDscto3(tx) {
    var categoria = $.trim(dataProductoDetalle.categoria);
    tx.executeSql("Select * from Descuentos where Tipo='D3' and Lista=? and Valor!=0", [parseInt(categoria)], function (tx, result) {
        var ncli = result.rows.length;
        var html = [];
        html.push('<option value="0" data-min="0" data-max="0">0%</option>');
        if (ncli > 0) {
            $('#select-desc3').empty();
            for (var i = 0; i < ncli; i++) {
                var row = result.rows.item(i);
                html.push('<option value="' + row['Valor'] + '" data-min="' + row['Min_m'] + '" data-max="' + row['Max_m'] + '">' + row['Valor'] + '%</option>');
            }
        } else {
            console.log("no hay datos forma de pago");
        }
        $('#select-desc3').html(html.join(''));
    });
}
function listarListaPrecios(tx) {

    tx.executeSql("select CodigoItem,CodigoUM,CodigoLPrecio,PrecioVenta,Descripcion,Commoditie1,Commoditie2 from DetalleListaPrecios where CodigoItem=? ", [dataProductoDetalle.codigo], function (tx, result) {
        var ncli = result.rows.length;
        if (ncli > 0) {
            $('#select-precios').empty();
            var html = [];
            if (ncli === 1) {
                var row = result.rows.item(0);
                html.push('<option value="' + row['CodigoLPrecio'] + '" data-precio="' + row['PrecioVenta'] + '" data-commoditie1="' + row['Commoditie1'] + '" data-commoditie2="' + row['Commoditie2'] + '">s/.' + row['PrecioVenta'] + ' - ' + row['Descripcion'] + '</option>');

            } else {
                html.push('<option value>--Seleccionar--</option>');
                for (var i = 0; i < ncli; i++) {
                    var row = result.rows.item(i);
                    html.push('<option value="' + row['CodigoLPrecio'] + '" data-precio="' + row['PrecioVenta'] + '"  data-commoditie1="' + row['Commoditie1'] + '" data-commoditie2="' + row['Commoditie2'] + '">s/.' + row['PrecioVenta'] + ' - ' + row['Descripcion'] + '</option>');
                }
            }
            $('#select-precios').html(html.join(''));

        } else {
            console.log("no hay datos en lista de precios");
        }
    });
}

var id_li_activo = '';
var array_cabecera_p = [];
var lastInsertId = 0;
var correcto = 0;

jQuery('#btn_guaradar_pedido').on('vclick', function () {


//    jQuery('#btn_aceptar_cabecera').trigger('click');

//VALIDACIONES CABECERA
//    activarPanel('detalle');


    $("#btn_guaradar_pedido").prop("disabled", true);
    var direccion = jQuery('#select-direccion').val();
    if (direccion === undefined || direccion === '' || direccion === '--Seleccione--') {
        alerta_error_msg("Seleccione una dirección de facturación.");
        $('#btn_aceptar_cabecera').removeAttr('disabled');
        return false;
    }
    var direccion_envio = jQuery('#select-direccion-ennvio').val();
    if (direccion_envio === undefined || direccion_envio === '' || direccion_envio === '--Seleccione--') {
//        alerta_error_msg("Seleccione una dirección de envío.");
//        $('#btn_aceptar_cabecera').removeAttr('disabled');
//        return false;
        direccion_envio = '';
    }

    var forma_pago = jQuery('#select-forma-pago').val();
    if (forma_pago === undefined || forma_pago === '' || forma_pago === '--Seleccione--') {
        alerta_error_msg("Seleccione una forma de pago.");
        $('#btn_aceptar_cabecera').removeAttr('disabled');
        return false;
    }

    var letras = jQuery('#frm_cantidad_letras').val();
    var obletras = jQuery('#txtObsLetras').val();
    if (forma_pago === 'L06' || forma_pago === 'L07') {
        var bancos = jQuery('#select-bancos').val();

        if (letras === undefined || letras === '' || letras === '--Seleccione--') {
            alerta_error_msg("Ingrese el numero de letras.");
            $('#btn_aceptar_cabecera').removeAttr('disabled');
            return false;
        }

    }


//    var rp = confirm('¿Está seguro que desea grabar el pedido?');
//
//    if (rp === false) {
//        return false;
//    }

    var frm_agencia = jQuery('#select-agencia').val();
    if (frm_agencia === undefined || frm_agencia === '') {
        frm_agencia = '000';
    }
    var letras = jQuery('#frm_cantidad_letras').val();
    var obletras = jQuery('#txtObsLetras').val();



    if (obletras === undefined || obletras === '' || obletras === '--Seleccione--') {
        obletras = '';
    }

    var hoy = new Date();
    var time = hoy.getTime();

    var uuid_movil;
    uuid_movil = "";

    if (getCurrentUser() === false) {
        if (produccion) {
            navigator.notification.alert(
                    'No se han identificado, por favor ingrese su usuario y contraseña.', // message
                    alertDismissed, // callback
                    '', // title
                    'Aceptar'                  // buttonName
                    );
        } else {
            alert('No se han identificado, por favor ingrese su usuario y contraseña.');
        }
        $("#btn_guaradar_pedido").prop("disabled", false);
        return;
    }

    if (jQuery('#productos-pedido-lista-detalle li').length === 0) {

        if (produccion) {
            navigator.notification.alert(
                    'Por favor ingrese algunos productos.', // message
                    alertDismissed, // callback
                    '', // title
                    'Aceptar'                  // buttonName
                    );
        } else {
            alert('Por favor ingrese algunos productos');
        }

        $("#btn_guaradar_pedido").prop("disabled", false);
        return;
    }

    var productos_marcados = jQuery('#productos-pedido-lista-detalle li a[href=#demo-mail]');

    if (productos_marcados.length === 0) {

        if (produccion) {
            navigator.notification.alert(
                    'Por favor  seleccione algún productos.', // message
                    alertDismissed, // callback
                    '', // title
                    'Aceptar'                  // buttonName
                    );
        } else {
            alert('Por favor  seleccione algún productos');
        }
        $("#btn_guaradar_pedido").prop("disabled", false);
        return;
    }

    var rp = confirm('¿Está seguro que desea grabar el pedido?');

    if (rp === false) {
        return false;
    }

    var pedido_detalle = [];
    var i = 0;
    var row_pedido_detalle = null;
    var ValorCantidad, ValorBruto, ValorDscto, ValorSubTotal, ValorIGV,
            ValorTotal, ValorDscto1, ValorDscto2, ValorDscto3, ValorDetalle1,
            ValorDetalle2, ValorDetalle3, ValorDsctoSucesivo,
            ValorPrecio;


    for (i = 0; i < productos_marcados.length; i++) {
        //Valor de descuento sucesivo por defecto 0 hasta consultar
        ValorDsctoSucesivo = 0;
        //Llenar con valor del producto
        ValorPrecio = parseFloat(jQuery(productos_marcados[i]).data('precio'));
        //Llenar con valor de cantidad
        ValorCantidad = parseFloat(jQuery(productos_marcados[i]).data('cantidad'));
        if (isNaN(ValorCantidad)) {
            ValorCantidad = 0;
        }
        //LLenar con % de descuento tipo D1
        ValorDscto1 = parseFloat(jQuery(productos_marcados[i]).data('dscto1')) / 100;
        //LLenar con % de descuento tipo D2
        ValorDscto2 = parseFloat(jQuery(productos_marcados[i]).data('dscto2')) / 100;
        //LLenar con % de descuento tipo D3
        ValorDscto3 = parseFloat(jQuery(productos_marcados[i]).data('dscto3')) / 100;
        /////////////////////////////////////////////////
        //Valor bruto
        ValorBruto = ValorPrecio * ValorCantidad;
        //Valor detalle de descuento 1
        ValorDetalle1 = ValorDscto1 * ValorBruto;
        //Valor detalle de descuento 2
        ValorDetalle2 = ValorDscto2 * ValorBruto;
        //Valor detalle de descuento 3
        ValorDetalle3 = ValorDscto3 * ValorBruto;
        //Valor de descuento
        ValorDscto = ValorDetalle1 + ValorDetalle2 + ValorDetalle3 + ValorDsctoSucesivo;
        //Valor Subtotal
        ValorSubTotal = ValorBruto - ValorDscto;
        //Valor IGV
        ValorIGV = ValorSubTotal * 0.18;
        //Valor total
        ValorTotal = ValorSubTotal + ValorIGV;

        row_pedido_detalle = {
            "id_tmp_exece": jQuery(productos_marcados[i]).data('id-obj-tmp'),
            "CodCategoria": jQuery(productos_marcados[i]).data('categoria'),
            "CodProducto": jQuery(productos_marcados[i]).data('codigo'),
            "CodListaPrecio": jQuery(productos_marcados[i]).data('codigoprecio'),
            "Cantidad": jQuery(productos_marcados[i]).data('cantidad'),
            "D1": parseFloat(jQuery(productos_marcados[i]).data('dscto1')),
            "D1_Max": parseFloat(jQuery(productos_marcados[i]).data('d1-max')),
            "D1_Min": parseFloat(jQuery(productos_marcados[i]).data('d1-min')),
            "DetalleD1": ValorDetalle1,
            "D2": parseFloat(jQuery(productos_marcados[i]).data('dscto2')),
            "DetalleD2": ValorDetalle2,
            "D3": jQuery(productos_marcados[i]).data('dscto3'),
            "DetalleD3": ValorDetalle3,
            "ValorBruto": ValorBruto,
            "ValorDscto": ValorDscto,
            "SubTotal": ValorSubTotal,
            "ValorIGV": ValorIGV,
            "ValorTotal": ValorTotal,
            "Presentacion": jQuery(productos_marcados[i]).data('unidad'),
            "DsctoSucesivo": ValorDsctoSucesivo,
            "Precio": ValorPrecio
        };

        var formapago = jQuery('#select-forma-pago').val();

        console.debug(formapago);
        if (!(formapago === 'CON' || formapago === 'PAD')) {
            if (row_pedido_detalle.D2 > 0) {

                jQuery('#productos-pedido-lista-detalle a[data-id-obj-tmp=' + row_pedido_detalle.CodProducto + ']').css('background-color', 'red');
                jQuery('#productos-pedido-lista-detalle a[data-id-tmp=' + row_pedido_detalle.CodProducto + ']').css('background-color', 'red');
                alert('Hay productos que no se les puede aplicar el descuento.');
                $("#btn_guaradar_pedido").prop("disabled", false);
                return false;
            }
        }

        pedido_detalle.push(row_pedido_detalle);
    }
    var fecha_fin = fechaInicial();
    var obj_pedido = {
        pedido: {
            cabecera: {
                uuid: uuid_movil,
                "CodVendedor": getCurrentUser(),
                "CodCanalVenta": "0001",
                "CodCliente": dataCliente.id,
                "CodTipoDoc": jQuery('input[name=radio-choice-h-2]:checked').val(),
                "CodFormaPago": jQuery('#select-forma-pago').val(),
                "CodMoneda": "SOL",
                "Letras": letras,
                "ObsLetras": obletras,
                "CodAgencia": frm_agencia,
                "DirEntrega": direccion_envio,
                "DirFacturacion": jQuery('#select-direccion').val(),
//                "FechaEntrega": jQuery('#txt_fecha_entrega').val(),
                "Observaciones": jQuery('#txtObsPedido').val(),
                "MntSubTotal": ValorSubTotal,
                "MntIGV": ValorIGV,
                "MntTotal": ValorTotal,
                "CodMotivoNoPedido": "000",
                "FechaInicio": dataCliente.fechainicio,
                "FechaFin": fecha_fin,
                "FechaDB": fecha_fin,
                "FlgMigracion": "T",
                "MntBruto": ValorBruto,
                "MntDscto": ValorDscto,
                "Latitud": position_gps_x.coords.latitude,
                "Longitud": position_gps_x.coords.longitude,
                "GuiaRemision": jQuery('input[name=tipo_guia_]:checked').val(),
                "Vcto_letra": jQuery('#frm_vcto_letra').val(),
                "Vcto_letra_sig": jQuery('#frm_vcto_letra_sig').val(),
                "FechaEntrega": fecha_fin,
            },
            detalle: pedido_detalle,
        }
    };

    var i = 0;
    var d1 = 0;
    var max = 0;
    var min = 0;
    var error_flg = 0;

    for (i; i < pedido_detalle.length; i++) {

        d1 = parseFloat(pedido_detalle[i].D1);
        max = parseFloat(pedido_detalle[i].D1_Max);
        min = parseFloat(pedido_detalle[i].D1_Min);

        if (d1 > 0) {
            console.debug(parseFloat(obj_pedido.pedido.cabecera.MntTotal));
            if (parseFloat(obj_pedido.pedido.cabecera.MntTotal) < min) {
                jQuery('#productos-pedido-lista-detalle a[data-id-tmp=' + pedido_detalle[i].id_tmp_exece + ']').parent().find('a').css('background-color', 'red');
                alert('No se pueden efectuar todos los descuentos por no llegar al monto mínimo.');
                $("#btn_guaradar_pedido").prop("disabled", false);
                error_flg++;
                return false;
            }
        }
    }

    if (error_flg > 0) {
        console.log("flag");
        return false;

    }

    if (obj_pedido.pedido.cabecera.DirEntrega.length === 0) {
        alert('Por favor ingrese una dirección de entrega');
        $("#btn_guaradar_pedido").prop("disabled", false);
        jQuery('#select-direccion-ennvio').focus();
        return false;
    }

    if (obj_pedido.pedido.cabecera.DirFacturacion.length === 0) {
        alert('Por favor ingrese una dirección de envío');
        $("#btn_guaradar_pedido").prop("disabled", false);
        jQuery('#select-direccion-direccion').focus();
        return false;
    }

    if (obj_pedido.pedido.cabecera.GuiaRemision.length === 0) {
        alert('Por favor ingrese si desea guia de remisión');
        $("#btn_guaradar_pedido").prop("disabled", false);
//        jQuery('#select-direccion-ennvio').focus();
        return false;
    }
    if (obj_pedido.pedido.cabecera.CodTipoDoc.length + '' === 0) {
        alert('Por favor seleccione el tipo de Documento');
        $("#btn_guaradar_pedido").prop("disabled", false);
//        jQuery('#select-direccion-ennvio').focus();
        return false;
    }

    // Enviar al WS

//    console.debug(JSON.stringify(obj_pedido));

//    load_save();
    var conn = checkConnection();
    var id_syncronizado = 0;
    if (conn === true) {
        jQuery.ajaxSetup({
            contentType: "application/json; charset=utf-8",
            async: false,
            cache: false
        });

        $.mobile.loading('show', {
            textVisible: true,
            html: '<p>Enviando pedido ...</p>'
        });

        jQuery.ajax({
            url: config.rest_url + '/ws_pedidos',
            data: JSON.stringify(obj_pedido),
            method: 'POST',
            type: 'json',
            success: function (xhr) {

                $.mobile.loading('hide');
                id_syncronizado = xhr.id;

//            if (parseInt(xhr.id) > 0) {
//                if (produccion) {
//                    navigator.notification.alert(
//                            'Se guardó el pedido de manera correcta.', // message
//                            alertDismissed, // callback
//                            '', // title
//                            'Aceptar'                  // buttonName
//                            );
//                } else {
//                    alert('Se guardó el pedido de manera correcta.');
//                }
////                app.cargarPaginaPedidos();
//            }
            }
        });
    }
    //Guardar en local
    /////////////////////////////////////
    //CABECERA///////////////////////

    var sql_insert_cabecera = 'insert into CabeceraPedido (' +
            'CodVendedor,' +
            'CodCanalVenta,' +
            'CodCliente,' +
            'CodTipoDoc,' +
            'CodFormaPago,' +
            'CodMoneda,' +
            'Letras,' +
            'ObsLetras,' +
            'Vcto_letra,' +
            'Vcto_letra_sig,' +
            'CodAgencia,' +
            'DirEntrega,' +
            'DirFacturacion,' +
            'FechaEntrega,' +
            'Observaciones,' +
            'MntSubTotal,' +
            'MntIGV,' +
            'MntTotal,' +
            'CodMotivoNoPedido,' +
            'FechaInicio,' +
            'FechaFin,' +
            'FechaDB,' +
            'FlgMigracion,' +
            'MntBruto,' +
            'MntDscto,' +
            'Latitud,' +
            'Longitud, GuiaRemision, id_pedido_remoto) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    //Guardar detalle
    var array_detalle_pedido = [];
    var sql_detalle_pedido = 'insert into DetallePedido (' +
            'IdPedido,' +
            'CodCategoria,' +
            'CodProducto,' +
            'CodListaPrecio,' +
            'Cantidad,' +
            'D1,' +
            'DetalleD1,' +
            'D2,' +
            'DetalleD2,' +
            'D3,' +
            'DetalleD3,' +
            'ValorBruto,' +
            'ValorDscto,' +
            'SubTotal,' +
            'ValorIGV,' +
            'ValorTotal,' +
            'Presentacion,' +
            'DsctoSucesivo,' +
            'Precio,id_pedido_remoto) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    app.db.transaction(function (tx) {
        array_cabecera_p = [
            getCurrentUser(),
            "0001",
            dataCliente.id,
            jQuery('input[name=radio-choice-h-2]:checked').val(),
            jQuery('#select-forma-pago').val(),
            "SOL",
            jQuery('#frm_cantidad_letras').val(),
            '',
            jQuery('#frm_vcto_letra').val(),
            jQuery('#frm_vcto_letra_sig').val(),
            jQuery('#select-agencia').val(),
            jQuery('#select-direccion-ennvio').val(),
            jQuery('#select-direccion').val(),
            '',
            jQuery('#txtObsPedido').val(),
            ValorSubTotal,
            ValorIGV,
            ValorTotal,
            "000",
            dataCliente.fechainicio,
            fecha_fin,
            fecha_fin,
            "T",
            ValorBruto,
            ValorDscto,
            position_gps_x.coords.latitude,
            position_gps_x.coords.longitude,
            jQuery('input[name=tipo_guia_]:checked').val(),
            id_syncronizado,
        ];

        tx.executeSql(sql_insert_cabecera, array_cabecera_p, function (tx, result) {
            if (parseInt(result.insertId) !== 0) {
                lastInsertId = result.insertId;
                for (var i = 0; i < pedido_detalle.length; i++) {
                    array_detalle_pedido = [
                        lastInsertId,
                        pedido_detalle[i]["CodCategoria"],
                        pedido_detalle[i]["CodProducto"],
                        pedido_detalle[i]["CodListaPrecio"],
                        pedido_detalle[i]["Cantidad"],
                        pedido_detalle[i]["D1"],
                        pedido_detalle[i]["DetalleD1"],
                        pedido_detalle[i]["D2"],
                        pedido_detalle[i]["DetalleD2"],
                        pedido_detalle[i]["D3"],
                        pedido_detalle[i]["DetalleD3"],
                        pedido_detalle[i]["ValorBruto"],
                        pedido_detalle[i]["ValorDscto"],
                        pedido_detalle[i]["SubTotal"],
                        pedido_detalle[i]["ValorIGV"],
                        pedido_detalle[i]["ValorTotal"],
                        pedido_detalle[i]["Presentacion"],
                        pedido_detalle[i]["DsctoSucesivo"],
                        pedido_detalle[i]["Precio"],
                        id_syncronizado,
                    ];
                    tx.executeSql(sql_detalle_pedido, array_detalle_pedido, app.onSuccessPedido(), app.onError);
                }
                $.mobile.loading('hide');
//                $.mobile.changePage("#detalle_cliente");
                $.mobile.changePage("#clientes");
            }


        }, app.onError);

    });

    var msg_success_pedido = '';
    if (parseInt(id_syncronizado) === 0) {
        msg_success_pedido = 'Se guardó el pedido de manera correcta, en manera local, por favor cuanto tenga conexión sincronice.';
    } else {
        msg_success_pedido = 'Se guardó el pedido de manera correcta';
    }


    if (produccion) {
        navigator.notification.alert(
                msg_success_pedido, // message
                alertDismissed, // callback
                '', // title
                'Aceptar'                  // buttonName
                );
    } else {
        alert(msg_success_pedido);
    }

    $("#btn_guaradar_pedido").prop("disabled", false);


});





jQuery('#btn_header_guardar').on('vclick', function () {
    jQuery('#btn_guaradar_pedido').trigger('vclick');
});
jQuery('#btn_header_agregar_prod').on('vclick', function () {

//    dataProductoDetalle = null;
    producto_tmp = null;


    jQuery('#btn_popup_productos').trigger('click');
});
function updateDetallePedido(idpedido, idupdate) {
    app.db.transaction(function (tx) {
        tx.executeSql('update CabeceraPedido set id_pedido_remoto=? where idPedido=?', [idpedido, idupdate], app.onSuccess, app.onError);
        tx.executeSql('update DetallePedido set id_pedido_remoto=? where IdPedido=?', [idpedido, idupdate], app.onSuccess, app.onError);
    });
}



jQuery('#btn_popup_productos').on('vclick', function (evt) {
    jQuery('#hdn_flg_nuevo_producto').val('0');
});



