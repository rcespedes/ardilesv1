var config = {
    rest_url: 'http://52.4.34.166/apigility/public'
};

var producto_tmp = null;

app.onSuccess = function () {

};
app.onSuccessPedido = function () {

};

app.onError = function () {

};


var position_gps = 0;
var position_gps_x = 0;

function isLoged() {
    var rp = window.localStorage.getItem("login");
    if (parseInt(rp) === 1) {
        return true;
    } else {
        return false;
    }
}

function logout() {

    window.localStorage.setItem("login", 0);
    window.localStorage.clear();
}

function getCurrentUser() {
    var rp = window.localStorage.getItem("user");
    if (rp === '') {
        return false;
    }
    return rp;
}

function getCoords() {

    var pos, err;
    navigator.geolocation.getCurrentPosition(function (position) {
        pos = position;

        alert('Success -->' + JSON.stringify(position));

    }, function (error) {
        alert('Error -->' + JSON.stringify(error));
        err = error;
    });

    return pos;
}

var watchId = navigator.geolocation.watchPosition(getCoordsSuccess, getCoordsError);

function getCoordsSuccess(position) {
    position_gps = JSON.stringify(position);

//    alert(position_gps);
//    alert(JSON.stringify(position.coords));
//    alert(JSON.stringify(position.coords.latitude));
//    alert(JSON.stringify(position.coords.longitude));
//    alert(position_gps.coords.latitude);
//    alert(position_gps.coords.longitude);
    position_gps_x = position;
}
;

function getCoordsError(error) {
    alert('Ha ocurrio un error por favor tome nota e informe al administrador del sistema' + JSON.stringify(error));
}
;

function checkConnection() {
    if (produccion) {
        var networkState = window.navigator.connection.type;
        var states = {};
        states[Connection.UNKNOWN] = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI] = 'WiFi connection';
        states[Connection.CELL_2G] = 'Cell 2G connection';
        states[Connection.CELL_3G] = 'Cell 3G connection';
        states[Connection.CELL_4G] = 'Cell 4G connection';
        states[Connection.CELL] = 'Cell generic connection';
        states[Connection.NONE] = 'No network connection';

        if (networkState === Connection.NONE) {
            return false;
        }
        if (networkState === Connection.UNKNOWN) {
            return false;
        }
    } else {
        return true;
    }

    return true;
}