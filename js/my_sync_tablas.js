app.eliminarTablas = function () {
    console.log("init...eliminando tablas...");
    loadingMsgv2("Eliminando tablas..");
    app.db.transaction(function (tx) {
        tx.executeSql('DROP TABLE IF EXISTS DetalleListaPrecios;', []);
        tx.executeSql('DROP TABLE IF EXISTS Descuentos;', []);
        tx.executeSql('DROP TABLE IF EXISTS log_sync;', []);
        tx.executeSql('DROP TABLE IF EXISTS clientes;', []);
        tx.executeSql('DROP TABLE IF EXISTS producto;', []);
        tx.executeSql('DROP TABLE IF EXISTS CabeceraPedido;', []);
        tx.executeSql('DROP TABLE IF EXISTS FormaPago;', []);
        tx.executeSql('DROP TABLE IF EXISTS Direccioncliente;', []);
        tx.executeSql('DROP TABLE IF EXISTS Bancos;', []);
        tx.executeSql('DROP TABLE IF EXISTS MotivoNoVenta;', []);
        tx.executeSql('DROP TABLE IF EXISTS Agencias;', []);
        tx.executeSql('DROP TABLE IF EXISTS DetallePedido;', []);
    });
    console.log("fin...eliminando tablas...");
}
//Creando tablas para sincronizar
app.crearTablas = function () {
    loadingMsgv2("Creando tablas..");
    console.log("init... creando tablas...");
    app.db.transaction(function (tx) {
        tx.executeSql('CREATE TABLE DetalleListaPrecios (' +
                'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'IdDetalleListaPrecio INTEGER NOT NULL,' +
                'CodigoItem TEXT NULL,' +
                'CodigoUM TEXT NULL,' +
                'CodigoLPrecio TEXT NULL,' +
                'PrecioVenta DECIMAL NULL,' +
                'PrecioOferta DECIMAL NULL,' +
                'Descripcion TEXT NULL,' +
                'Commoditie1 INTEGER NULL,' +
                'Commoditie2 INTEGER NULL' +
                ')', []);
        tx.executeSql('CREATE TABLE Descuentos (' +
                'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'Tipo TEXT NOT NULL,' +
                'Lista INTEGER NOT NULL,' +
                'Valor INTEGER NOT NULL,' +
                'Min_m INTEGER NOT NULL,' +
                'Max_m INTEGER NOT NULL' +
                ')', []);
        tx.executeSql("CREATE TABLE IF NOT EXISTS log_sync (" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "CodVendedor TEXT NOT NULL," +
                "fecha_sync TEXT NOT NULL," +
                "tipo TEXT NOT NULL" +
                ")", []);
        tx.executeSql('CREATE TABLE clientes (' +
                'id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'IdCliente   INTEGER NOT NULL,' +
                'Codigo  TEXT NOT NULL,' +
                'Descripcion TEXT NOT NULL,' +
                'CodCategoria    TEXT,' +
                'Ruc TEXT,' +
                'Dni TEXT,' +
                'LimiteCredito   REAL NOT NULL,' +
                'LimiteCreditoDisp   REAL NOT NULL,' +
                'CodFormaPago    TEXT,' +
                'CodVendedor TEXT NOT NULL,' +
                'FlgRegHabilitado    TEXT NOT NULL,' +
                'CodAgencia  TEXT,' +
                'CodTipoNegocio  TEXT,' +
                'DescripcionNegocio  TEXT,' +
                'Agencia  TEXT,' +
                'Correo  TEXT,' +
                'Telefono  TEXT' +
                ')', []);
        tx.executeSql('CREATE TABLE producto (' +
                'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'IdProducto INTEGER NOT NULL,' +
                'Codigo TEXT NOT NULL,' +
                'Descripcion TEXT NOT NULL,' +
                'Presentacion TEXT,' +
                'Stock TEXT NOT NULL,' +
                'CodCategoria TEXT NOT NULL,' +
                'FlgRegHabilitado TEXT NOT NULL,' +
                'Categoria TEXT NOT NULL' +
                ')', []);
        tx.executeSql('CREATE TABLE CabeceraPedido (' +
                'IdPedido INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'CodVendedor TEXT NOT NULL,' +
                'CodCanalVenta TEXT NOT NULL,' +
                'CodCliente TEXT NOT NULL,' +
                'CodTipoDoc TEXT NULL DEFAULT NULL,' +
                'CodFormaPago TEXT NULL DEFAULT NULL,' +
                'CodMoneda TEXT NULL DEFAULT NULL,' +
                'Letras TEXT NULL DEFAULT NULL,' +
                'ObsLetras TEXT NULL DEFAULT NULL,' +
                'Vcto_letra INTEGER NULL DEFAULT 0,' +
                'Vcto_letra_sig INTEGER NULL DEFAULT 0,' +
                'CodAgencia TEXT NULL DEFAULT NULL,' +
                'DirEntrega TEXT NULL DEFAULT NULL,' +
                'DirFacturacion TEXT NULL DEFAULT NULL,' +
                'FechaEntrega TEXT NULL DEFAULT NULL,' +
                'Observaciones TEXT NULL DEFAULT NULL,' +
                'MntSubTotal REAL NOT NULL,' +
                'MntIGV REAL NOT NULL,' +
                'MntTotal REAL NOT NULL,' +
                'CodMotivoNoPedido TEXT NULL DEFAULT NULL,' +
                'FechaInicio TEXT NOT NULL,' +
                'FechaFin TEXT NOT NULL,' +
                'FechaDB TEXT NOT NULL,' +
                'FlgMigracion TEXT NULL DEFAULT NULL,' +
                'MntBruto REAL NOT NULL,' +
                'MntDscto REAL NOT NULL,' +
                'Latitud TEXT DEFAULT NULL,' +
                'Longitud TEXT DEFAULT NULL,' +
                'GuiaRemision TEXT DEFAULT NULL,' +
                'id_pedido_remoto INTEGER NOT NULL DEFAULT 0' +
                ')', []);
        tx.executeSql('CREATE TABLE FormaPago (' +
                'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'IdFormaPago INTEGER NOT NULL,' +
                'Codigo TEXT NOT NULL,' +
                'Descripcion TEXT NOT NULL,' +
                'GrupoForPago TEXT NOT NULL,' +
                'FlgDescuento3 TEXT NOT NULL,' +
                'FlgRegHabilitado TEXT DEFAULT NULL' +
                ')', []);
        tx.executeSql('CREATE TABLE Direccioncliente (' +
                'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'Codigo TEXT NOT NULL,' +
                'Descripcion TEXT NOT NULL,' +
                'TipoDireccion INTEGER NOT NULL' +
                ')', []);
        tx.executeSql('CREATE TABLE Bancos (' +
                'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'Codigo TEXT NOT NULL,' +
                'NombreLargo TEXT NOT NULL,' +
                'NombreCorto TEXT NOT NULL,' +
                'FlgRegHabilitado TEXT NOT NULL' +
                ')', []);
        tx.executeSql('CREATE TABLE MotivoNoVenta (' +
                'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'IdMotivoNoPedido INTEGER NOT NULL,' +
                'Codigo TEXT NOT NULL,' +
                'Descripcion TEXT NOT NULL,' +
                'abreviatura TEXT NOT NULL,' +
                'FlgRegHabilitado TEXT NOT NULL' +
                ')', []);
        tx.executeSql('CREATE TABLE Agencias (' +
                'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'IdAgencia INTEGER NOT NULL,' +
                'Codigo TEXT NOT NULL,' +
                'Descripcion TEXT NOT NULL,' +
                'FlgRegHabilitado TEXT NOT NULL' +
                ')', []);
        tx.executeSql('CREATE TABLE DetallePedido (' +
                'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'IdPedido INTEGER NOT NULL,' +
                'CodCategoria TEXT NOT NULL,' +
                'CodProducto TEXT NOT NULL,' +
                'CodListaPrecio TEXT NOT NULL,' +
                'Cantidad TEXT NOT NULL,' +
                'D1 TEXT NULL DEFAULT NULL,' +
                'DetalleD1 TEXT NULL DEFAULT NULL,' +
                'D2 TEXT NULL DEFAULT NULL,' +
                'DetalleD2 TEXT NULL DEFAULT NULL,' +
                'D3 TEXT NULL DEFAULT NULL,' +
                'DetalleD3 TEXT NULL DEFAULT NULL,' +
                'ValorBruto TEXT NULL DEFAULT NULL,' +
                'ValorDscto TEXT NULL DEFAULT NULL,' +
                'SubTotal TEXT NULL DEFAULT NULL,' +
                'ValorIGV TEXT NOT NULL,' +
                'ValorTotal TEXT NOT NULL,' +
                'Presentacion TEXT NULL DEFAULT NULL,' +
                'DsctoSucesivo TEXT NULL DEFAULT NULL,' +
                'Precio TEXT NOT NULL,' +
                'id_pedido_remoto INTEGER NOT NULL DEFAULT 0' +
                ')', []);
    });
    console.log("fin... creando tablas...");
}
//Insertando datos para login en forma local
app.consumeLogin = function () {
    app.db.transaction(function (tx) {
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0001", "0001", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0003", "0003", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0006", "0006", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0007", "0007", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0009", "0009", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0010", "0010", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0011", "0011", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0015", "0015", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0025", "0025", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0033", "0033", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0040", "0040", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0053", "0053", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0054", "0054", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0055", "0055", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0061", "0061", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0062", "0062", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0065", "0065", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0066", "0066", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0068", "0068", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0071", "0071", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0081", "0081", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0082", "0082", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0087", "0087", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0101", "0101", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0102", "0102", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0103", "0103", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0105", "0105", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0107", "0107", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0108", "0108", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0111", "0111", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0112", "0112", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0119", "0119", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0120", "0120", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0121", "0121", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0123", "0123", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0124", "0124", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0126", "0126", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0127", "0127", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0129", "0129", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0130", "0130", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0132", "0132", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0133", "0133", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0134", "0134", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0135", "0135", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0136", "0136", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0138", "0138", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0140", "0140", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0141", "0141", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0142", "0142", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0144", "0144", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0145", "0145", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0146", "0146", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0147", "0147", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0148", "0148", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0149", "0149", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0150", "0150", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0151", "0151", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0152", "0152", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0153", "0153", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0154", "0154", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0155", "0155", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0156", "0156", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0157", "0157", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0158", "0158", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0159", "0159", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0160", "0160", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0161", "0161", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0162", "0162", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0163", "0163", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0164", "0164", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0165", "0165", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0166", "0166", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0167", "0167", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0168", "0168", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0169", "0169", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0170", "0170", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0171", "0171", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0172", "0172", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0173", "0173", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0174", "0174", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0175", "0175", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0176", "0176", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0177", "0177", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0178", "0178", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0179", "0179", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0180", "0180", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0181", "0181", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0182", "0182", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0183", "0183", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0184", "0184", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0185", "0185", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0186", "0186", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0187", "0187", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0188", "0188", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("0189", "0189", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("123456", "123456", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("6666", "6666", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("7001", "7001", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("7002", "7002", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("7003", "7003", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("7004", "7004", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("7005", "7005", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("7777", "7777", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("8888", "8888", "T");');
        tx.executeSql('INSERT INTO "Login" ("Usuario", "Clave", "FlgRegHabilitado") VALUES ("9999", "9999", "T");');
    })
}
//Creando tabla login
app.createTableLogin = function () {
    app.db.transaction(function (tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS Login (' +
                'id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'Usuario TEXT NOT NULL,' +
                'Clave TEXT NOT NULL,' +
                'FlgRegHabilitado TEXT NOT NULL' +
                ')', []);
    });
}