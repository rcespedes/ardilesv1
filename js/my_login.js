function ingresarApp() {
    var usr = jQuery('#txt_user').val();
    var pss = jQuery('#txt_pass').val();
    var conn = checkConnection();
    //Verificar si existe conexión a internet
    if (!conn) {
        //no existe conexión
        navigator.notification.confirm(
                'No cuenta con internet, desea conectarse de manera local?', // message
                confirmaLogin, // callback to invoke with index of button pressed
                'Atención!', // title
                ['Aceptar', 'Cancelar']     // buttonLabels
                );
        return false;
    }
    validarLoginVps();

}
function cerrarApp() {
    navigator.app.exitApp();
}
function alertDismissed() {
    // do something
}
//Confirmar login de manera local
function confirmaLogin(btn) {
    if (btn === 1) {
        app.comprobarLoginLocal();
    } else {
        return;
    }
}

app.comprobarLoginLocal = function () {
    app.db.transaction(validarLoginLocal, app.onError);
}
//Validar login en el VPS
function validarLoginVps() {
    var usr = jQuery('#txt_user').val();
    var pss = jQuery('#txt_pass').val();
    $.ajaxSetup({
        contentType: "application/json; charset=utf-8",
        cache: false,
        async: true,
    });

    var obj = {user: usr, password: pss};
    jQuery.ajax({
        url: config.rest_url + '/ws_auth',
        data: JSON.stringify(obj),
        method: 'POST',
        type: 'json',
        success: function (xhr) {
            if (xhr.success === true) {
                if (produccion) {
                    navigator.notification.alert(
                            'Bienvenido(a): ' + xhr.nombres+', por favor inicie la sincronización del aplicativo. ', // message
                            alertDismissed, // callback
                            '', // title
                            'Aceptar'                  // buttonName
                            );


                } else {
                    alert('Bienvenido(a): ' + xhr.nombres+', por favor inicie la sincronización del aplicativo.');

                }
                window.localStorage.setItem("login", 1);
                window.localStorage.setItem("user", usr);
                $.mobile.changePage("#menupri");

            } else {
                if (produccion) {
                    navigator.notification.alert(
                            'No existe usuario logueado.', // message
                            alertDismissed, // callback
                            '', // title
                            'Aceptar'                  // buttonName
                            );
                } else {
                    alert('No existe usuario logueado.');
                }
                jQuery('#txt_user').val('');
                jQuery('#txt_pass').val('');
                window.localStorage.setItem("login", 0);
                window.localStorage.setItem("user", '');
                return;
            }
        }
    });

}

//Validar login en bd local
function validarLoginLocal(tx) {
    var usr = jQuery('#txt_user').val();
    var pss = jQuery('#txt_pass').val();
    tx.executeSql("Select * from Login where Usuario=? and clave=? ", [usr, pss], function (tx, result) {
        if (result.rows.length == 0) {
            if (produccion) {
                navigator.notification.alert(
                        'El usuario ingresado no existe.', // message
                        alertDismissed, // callback
                        '', // title
                        'Aceptar'                  // buttonName
                        );
            } else {
                alert('El usuario ingresado no existe.');
            }
        } else {
            window.localStorage.setItem("login", 1);
            window.localStorage.setItem("user", usr);
            $.mobile.changePage("#menupri");
        }
    });
}