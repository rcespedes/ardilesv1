var search_productop = '';
$(document).on('pageinit', '#productos', function () {
    //comprobar si existe tabla clientes
    app.comprobarBD();
    //app.cargarPaginaProductos();
    $("#autocomplete-input-productos").keyup(function () {
        search_productop = $(this).val();
        var tamanio = 0;
        tamanio = search_productop.length;
        if (tamanio > 2) {
            app.cargarPaginaProductos();
        }
    });

});
var dataProd = {
    id: null,
    codigo: null,
}
$(document).on('vclick', '#productos-lista li a', function () {
    dataProd.id = $(this).attr('data-id');
    dataProd.codigo = $(this).attr('data-codigo');

    $.mobile.changePage("#detalle_producto", {transition: "slide", changeHash: true});
});


app.cargarPaginaProductos = function () {
    console.log('metodo cargar productos');
    app.db.transaction(listarProductos, app.onError);
}

function listarProductos(tx) {
    console.log('metodo cargar  productos de table');
    tx.executeSql("Select * from producto where Descripcion like '%" + search_productop + "%'", [], function (tx, result) {
        var ncli = result.rows.length;
        if (ncli > 0) {
            $('#productos-lista').empty();
            for (var i = 0; i < ncli; i++) {
                var row = result.rows.item(i);
                $('#productos-lista').append('<li data-filtertext="' + row['Descripcion'] + '" ><a href="" data-codigo="' + row['Codigo'] + '" data-categoria="' + row['Categoria'] + '" data-id="' + row['id'] + '" data-desc="' + row['Descripcion'] + '" data-stock="' + row['Stock'] + '"><img src="img_ardiles/001.jpg"><p><b>' + row['Descripcion'] + '</p><p>Stock: ' + row['Stock'] + '</b></p></a></li>');
            }
            $('#productos-lista').listview('refresh');
        } else {
            console.log("no hay datos");
        }
    });
}

$(document).on('pagebeforeshow', '#detalle_producto', function () {
    app.db.transaction(listarDetalleProductos, app.onError);
});

function listarDetalleProductos(tx) {
    console.log('metodo cargar detalle productos de table');
    console.log(dataProd.codigo);
    tx.executeSql("Select * from producto where Codigo=?", [dataProd.codigo], function (tx, result) {
        var ncli = result.rows.length;
        console.log(result.rows.item);
        if (ncli > 0) {
            $('#productos-data').empty();
            var html = '';
            for (var i = 0; i < ncli; i++) {
                var row = result.rows.item(i);
                $('#productos-data').append('<li><b>Código: </b>' + row['Codigo'] + '</li>');
                $('#productos-data').append('<li><b>Descripcion: </b>' + row['Descripcion'] + '</li>');
                $('#productos-data').append('<li><b>Presentación: </b>' + row['Presentacion'] + '</li>');
                $('#productos-data').append('<li><b>Stock: </b>' + row['Stock'] + '</li>');
                $('#productos-data').append('<li><b>Categoría : </b>' + row['Categoria'] + '</li>');
            }
            $('#productos-data').listview('refresh');
        } else {
            console.log("no hay datos productos detalle");
        }
    });
}
