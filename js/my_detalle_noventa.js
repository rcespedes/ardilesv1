$(document).on('pageinit', '#detail_no_venta', function () {

    $('#btn_guardar_nopedido').click(function () {
        var val_rd = $('input[name=rd_motivo_noventa]:checked').val();
        if (val_rd !== undefined) {
            var fechafin = fechaInicial();
            var array_cabecera_p = [];
            var sql_insert_cabecera = 'insert into CabeceraPedido (' +
                    'CodVendedor,' +
                    'CodCanalVenta,' +
                    'CodCliente,' +
                    'CodTipoDoc,' +
                    'CodFormaPago,' +
                    'CodMoneda,' +
                    'Letras,' +
                    'ObsLetras,' +
                    'CodAgencia,' +
                    'DirEntrega,' +
                    'DirFacturacion,' +
                    'FechaEntrega,' +
                    'Observaciones,' +
                    'MntSubTotal,' +
                    'MntIGV,' +
                    'MntTotal,' +
                    'CodMotivoNoPedido,' +
                    'FechaInicio,' +
                    'FechaFin,' +
                    'FechaDB,' +
                    'FlgMigracion,' +
                    'MntBruto,' +
                    'MntDscto,' +
                    'Latitud,' +
                    'Longitud, GuiaRemision,id_pedido_remoto) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            app.db.transaction(function (tx) {
                array_cabecera_p = [
                    getCurrentUser(),
                    "0000",
                    dataCliente.id,
                    "",
                    0,
                    "",
                    0,
                    "",
                    0,
                    "",
                    "",
                    "",
                    "",
                    0,
                    0,
                    0,
                    val_rd,
                    dataCliente.fechainicio,
                    fechafin,
                    fechafin,
                    "T",
                    0,
                    0,
                    position_gps_x.coords.latitude,
                    position_gps_x.coords.longitude,
                    "",
                    "0",
                ];
                tx.executeSql(sql_insert_cabecera, array_cabecera_p, function (tx, result) {
                    if (parseInt(result.insertId) !== 0) {
                        var lastInsertId = result.insertId;
                        var obj_pedido = {
                            pedido: {
                                cabecera: {
                                    uuid: "",
                                    "CodVendedor": getCurrentUser(),
                                    "CodCanalVenta": "0001",
                                    "CodCliente": dataCliente.id,
                                    "CodTipoDoc": "",
                                    "CodFormaPago": 0,
                                    "CodMoneda": "",
                                    "Letras": 0,
                                    "ObsLetras": "",
                                    "CodAgencia": 0,
                                    "DirEntrega": "",
                                    "DirFacturacion": "",
                                    "FechaEntrega": "",
                                    "Observaciones": "",
                                    "MntSubTotal": 0,
                                    "MntIGV": 0,
                                    "MntTotal": 0,
                                    "CodMotivoNoPedido": val_rd,
                                    "FechaInicio": dataCliente.fechainicio,
                                    "FechaFin": fechafin,
                                    "FechaDB": fechafin,
                                    "FlgMigracion": "T",
                                    "MntBruto": 0,
                                    "MntDscto": 0,
                                    "Latitud": position_gps_x.coords.latitude,
                                    "Longitud": position_gps_x.coords.longitude,
                                    "Vcto_letra": jQuery('#frm_vcto_letra').val(),
                                    "Vcto_letra_sig": jQuery('#frm_vcto_letra_sig').val(),
                                    "FechaEntrega": fechafin,
                                    "GuiaRemision": 0,
                                },
                                detalle: [],
                            }
                        };

                        var conn = checkConnection();
                        /////////////////////////////////////////////////
                        //////////////SERVIDOR//////////////////////////
                        //Comprobar si existe conexion
                        if (conn) {
                            jQuery.ajaxSetup({
                                contentType: "application/json; charset=utf-8",
                                async: false,
                                cache: false
                            });
                            var id_syncronizado = 0;
                            jQuery.ajax({
                                url: config.rest_url + '/ws_pedidos',
                                data: JSON.stringify(obj_pedido),
                                method: 'POST',
                                type: 'json',
                                success: function (xhr) {
                                    id_syncronizado = xhr.id;
                                    $("#btn_guaradar_pedido").prop("disabled", false);
                                    if (parseInt(xhr.id) > 0) {
                                        updateDetallePedido(id_syncronizado, lastInsertId);
                                    }
                                }
                            });
                        }
                        if (produccion) {
                            navigator.notification.alert(
                                    'Se envió correctamente.', // message
                                    alertDismissed, // callback
                                    '', // title
                                    'Aceptar'                  // buttonName
                                    );
                        } else {
                            alert('Se envió correctamente.');
                        }
//                        $.mobile.changePage("#detalle_cliente", {transition: "slide", changeHash: true});
                        $.mobile.changePage("#menupri", {transition: "slide", changeHash: true});

                    }
                }, app.onError);
            })

        }
    });
});