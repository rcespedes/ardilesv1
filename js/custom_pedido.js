
//jQuery('#btn_ver_cabecera').on('vclick', function () {
//    activarPanel("cabecera");
//});
//jQuery('#btn_aceptar_cabecera').on('vclick', function () {
//    $('#btn_aceptar_cabecera').attr('disabled', 'disabled');
//
//    //VALIDACIONES CABECERA
//
//    var direccion = jQuery('#select-direccion').val();
//    if (direccion === undefined || direccion === '' || direccion === '--Seleccione--') {
//        alerta_error_msg("Seleccione una dirección de facturación.");
//        $('#btn_aceptar_cabecera').removeAttr('disabled');
//        return false;
//    }
//    var direccion_envio = jQuery('#select-direccion-ennvio').val();
//    if (direccion_envio === undefined || direccion_envio === '' || direccion_envio === '--Seleccione--') {
//        alerta_error_msg("Seleccione una dirección de envío.");
//        $('#btn_aceptar_cabecera').removeAttr('disabled');
//        return false;
//    }
//
//    var forma_pago = jQuery('#select-forma-pago').val();
//    if (forma_pago === undefined || forma_pago === '' || forma_pago === '--Seleccione--') {
//        alerta_error_msg("Seleccione una forma de pago.");
//        $('#btn_aceptar_cabecera').removeAttr('disabled');
//        return false;
//    }
//    activarPanel('detalle');
//
//
//});

$(document).on('pageinit', '#page_cabecera_pedido', function () {

    $('#cliente-data-pago').empty();
    $('#cliente-data-pago').append('<li style="text-align: center;"><b>' + dataCliente.desc + '</b></li>');
    $('#cliente-data-pago').listview('refresh');

    $("#select-forma-pago").change(function () {
        var str = $(this).val();
        if (str.charAt(0) === 'L') {
            jQuery('#frm_pago_letras').show();
        } else {
            jQuery('#frm_cantidad_letras').val('0');
            jQuery('#frm_pago_letras').hide();
        }
    });

});

$(document).on('vclick', '#btn_cancelar_pedido', function () {
    navigator.notification.confirm(
            'Por favor confirme si desea cancelar este producto?', // message
            confirmaCancelarPedido, // callback to invoke with index of button pressed
            'Atención!', // title
            ['Aceptar', 'Cancelar']     // buttonLabels
            );
    return false;
});

function confirmaCancelarPedido(btn) {
    if (btn === 1) {
        history.go(-1);
    } else {
        return;
    }
}

$(document).on('vclick', '#btn_aceptar_pedido', function () {
    var frm_cantidad = $('#frm_cantidad').val();
    var frm_d1 = $('#select-desc1').val();
    var frm_d2 = $('#select-desc2').val();
    var frm_d3 = $('#select-desc3').val();
    var frm_lista = $('#select-precios').val();
    var precio = $('#select-precios option:checked').data('precio');
    var commoditie1 = $('#select-precios option:checked').data('commoditie1');
    var commoditie2 = $('#select-precios option:checked').data('commoditie2');
    var d1_min = parseInt($('#select-desc1 option:checked').data('min'));
    var d1_max = parseInt($('#select-desc1 option:checked').data('max'));
    if (frm_cantidad === undefined || frm_cantidad === 0 || frm_cantidad === '') {
        if (produccion) {
            navigator.notification.alert(
                    'Debe ingresar una cantidad.',
                    alertDismissed,
                    '',
                    'Aceptar'
                    );
        } else {
            alert('Debe ingresar una cantidad.');
        }
        return false;
    }
    if (frm_d1 === undefined) {
        frm_d1 = 0;
    }
    if (frm_d2 === undefined) {
        frm_d2 = 0;
    }
    if (frm_d3 === undefined) {
        frm_d3 = 0;
    }
    if (frm_lista === undefined || frm_lista === '') {

        if (produccion) {
            navigator.notification.alert(
                    'Debe escoger un precio de la lista.',
                    alertDismissed,
                    '',
                    'Aceptar'
                    );
        } else {
            alert('Debe escoger un precio de la lista.');
        }
        return false;
    }
    //Validando commoditie

    if (frm_lista === 'CM2' && parseInt(frm_cantidad) < parseInt(commoditie1)) {
        if (produccion) {
            navigator.notification.alert(
                    'Debe escoger un precio de la lista COMMODITIE 1.',
                    alertDismissed,
                    '',
                    'Aceptar'
                    );
        } else {
            alert('Debe escoger un precio de la lista COMMODITIE 1.');
        }
        return false;
    }

    //validar que el producto no exista en la lista

    var flg_nuevo = parseInt(jQuery('#hdn_flg_nuevo_producto').val());

    console.debug(flg_nuevo);
    if (flg_nuevo === 1) {
        console.debug(flg_nuevo + 'xD - 123 ');
    } else {
        console.debug(flg_nuevo + 'xD -Valida ');
        var rpta = validaProductos(dataProductoDetalle.codigo);
        console.debug(rpta);
        if (rpta) {
            return false;
        }
    }
    if (producto_tmp !== null) {
        eliminar_producto_detalle2('#productos-pedido-lista-detalle a[data-id-tmp=' + producto_tmp.id_obj_tmp + ']', precio, producto_tmp.cantidad, producto_tmp.dscto1, producto_tmp.dscto2, producto_tmp.dscto3);
        producto_tmp = null;
    }

    var time_tmp = new Date();
//    var id_tmp = time_tmp.getTime();
    var id_tmp = dataProductoDetalle.codigo;
    var obj_tmp = calcular_precio_final(precio, frm_cantidad, frm_d1, frm_d2, frm_d3);

    $('#productos-pedido-lista-detalle').append('<li><a href="#demo-mail" data-id-obj-tmp="' + id_tmp + '" data-unidad="' + dataProductoDetalle.unidad +
//            '" data-codigoprecio="' + dataProductoDetalle.codigoprecio +
            '" data-codigoprecio="' + frm_lista +
//            '" data-precio="' + dataProductoDetalle.precio +
            '" data-precio="' + precio +
            '" data-d1-min="' + d1_min +
            '" data-d1-max="' + d1_max +
            '" data-categoria="' + dataProductoDetalle.categoria +
            '" data-idproducto="' + dataProductoDetalle.id +
            '" data-codigo="' + dataProductoDetalle.codigo +
            '" data-desccategoria="' + dataProductoDetalle.desccategoria +
            '" data-cantidad="' + frm_cantidad +
            '" data-codigo="' + dataProductoDetalle.codigo +
            '" data-desc="' + dataProductoDetalle.desc +
            '" data-stock="' + dataProductoDetalle.stock +
            '" data-unidad="' + dataProductoDetalle.unidad +
            '" data-dscto1="' + frm_d1 +
            '" data-dscto2="' + frm_d2 +
            '" data-dscto3="' + frm_d3 +
            '"><h3>Código:' + dataProductoDetalle.codigo +
            '</h3>\n\
            <p class="font-16">' + dataProductoDetalle.desc + '</p>\n\
            <p class="font-14">' + 'Precio: ' + numberWithCommas(obj_tmp.valorTotal.toFixed(2)) + '</p>\n\
            <p class="ui-li-aside"><strong>' + numberWithCommas(frm_cantidad) + dataProductoDetalle.unidad + '</strong></p>\n\
            </a>\n\
            <a href="#" class="delete" data-id-tmp="' + id_tmp + '">Delete</a></li>');

//            <p class="topic"><strong>' + dataProductoDetalle.desccategoria + '</strong></p>\n\
    $('#productos-pedido-lista-detalle').listview('refresh');
    actualizar_totales(1, precio, frm_cantidad, frm_d1, frm_d2, frm_d3);

    
    jQuery('#productos-pedido-lista-detalle a[data-id-obj-tmp=' + id_tmp + ']').on('vclick', function () {

        producto_tmp = {
            cantidad: jQuery(this).data('cantidad'),
            precio: jQuery(this).data('precio'),
            dscto1: jQuery(this).data('dscto1'),
            dscto2: jQuery(this).data('dscto2'),
            dscto3: jQuery(this).data('dscto3'),
            stock: jQuery(this).data('stock'),
            desc: jQuery(this).data('desc'),
            codigo: jQuery(this).data('codigo'),
            unidad: jQuery(this).data('unidad'),
            categoria: jQuery(this).data('categoria'),
//            categoria: jQuery(this).data('categoria'),
            id_obj_tmp: id_tmp,
        };

        console.debug(producto_tmp);
        dataProductoDetalle = producto_tmp;

        app.db.transaction(listarDscto1, app.onError);
        app.db.transaction(listarDscto2, app.onError);
        app.db.transaction(listarDscto3, app.onError);
        app.db.transaction(listarListaPrecios, app.onError);

        jQuery('#hdn_flg_nuevo_producto').val('1');
        $.mobile.changePage("#popupCantidad");

    });

    //ELIMINAR PEDIDO DEL DETALLE
    jQuery('#productos-pedido-lista-detalle .delete').off('vclick');
    jQuery('#productos-pedido-lista-detalle .delete').on('vclick', function (e) {

        var rp = confirm('Desea eliminar el detalle?')
        if (rp) {
            eliminar_producto_detalle(precio, frm_cantidad, frm_d1, frm_d2, frm_d3);
            jQuery(this).parent().remove();
        }

    });

    $.mobile.changePage("#page_cabecera_pedido");

});

function validaProductos(id) {
    var productos_marcados = jQuery('#productos-pedido-lista-detalle li a[href=#demo-mail]');
    var rpta = false;
    if (productos_marcados.length > 0)
    {
        for (i = 0; i < productos_marcados.length; i++)
        {
            if (jQuery(productos_marcados[i]).data('codigo') === id) {
                alert('Este producto ya se encuentra agregado en el detalle de pedido.');
                rpta = true;
                break;
            }
        }
    }

    return rpta;
}

function alerta_error_msg(txt) {
    if (produccion) {
        navigator.notification.alert(
                txt, // message
                alertDismissed, // callback
                '', // title
                'Aceptar'                  // buttonName
                );
    } else {
        alert(txt);
    }
}

function activarPanel(txt) {

    if (txt === 'cabecera') {
        $('#btn_ver_cabecera').blur();
        $('#dv_cabecera_pedido').show();
        $('#dv_detalle_pedido').hide();
        $('#btn_guaradar_pedido').attr('disabled', 'disabled');
//        $('#btn_det_guardar').hide();
        $('#btn_cab_aceptar').show();
        $('#btn_aceptar_cabecera').removeAttr('disabled');
    }

    if (txt === 'detalle') {
        $('#dv_cabecera_pedido').hide();
        $('#dv_detalle_pedido').show();
        $('#btn_aceptar_cabecera').attr('disabled', 'disabled');
        $('#btn_guaradar_pedido').removeAttr('disabled', 'disabled');
//        $('#btn_det_guardar').show();
        $('#btn_cab_aceptar').hide();
    }

}

function limpiarPanel(frm) {
    console.log("entre a limpiar panel " + frm);
    jQuery("#" + frm)[0].reset();
}