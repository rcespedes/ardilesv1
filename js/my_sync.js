
//jQuery('#btn_get_pos').on('click', function (evt) {
//    var position = getCoords();
//    alert("BTN ---> "+JSON.stringify(position));
//    
////    alert('Latitude: '          + position.coords.latitude          + '\n' +
////          'Longitude: '         + position.coords.longitude         + '\n' +
////          'Altitude: '          + position.coords.altitude          + '\n' +
////          'Accuracy: '          + position.coords.accuracy          + '\n' +
////          'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
////          'Heading: '           + position.coords.heading           + '\n' +
////          'Speed: '             + position.coords.speed             + '\n' +
////          'Timestamp: '         + position.timestamp                + '\n');
//});

var conta_sync = 0;
var percentaje_txt = 10;
//Mostrar loading
function loadingMsg(txt) {
    percentaje_txt += 10;
    var html_txt = '<p>Sincronización al ' + percentaje_txt + '% </p>';
    html_txt += '<p>' + txt + '</p>';
    $.mobile.loading('show', {
        textVisible: true,
        html: html_txt
    });
}
function loadingMsgv2(txt) {

    var html_txt = '<p>Iniciando sincronización... </p>';
    html_txt += '<p>' + txt + '</p>';
    $.mobile.loading('show', {
        textVisible: true,
        html: html_txt
    });
}

//Ocultar loading
function hideLoading(txt, estado) {
    console.log("INICIO DE CONTADOR" + conta_sync)
    conta_sync++;
    console.log("Contador sync: " + conta_sync + " -- " + txt);

    if (estado) {
        if (parseInt(conta_sync) === 9) {
            if (produccion) {
                navigator.notification.alert(
                        'Se sincronizó de manera correcta.', // message
                        alertDismissed, // callback
                        '', // title
                        'Aceptar'                  // buttonName
                        );
            } else {
                alert('Se sincronizó de manera correcta.');
            }
            $.mobile.loading('hide');
            $("#btn_guaradar_pedido").prop("disabled", false);
        }
    } else {
        if (produccion) {
            navigator.notification.alert(
                    'Error' + conta_sync + ' ----', // message
                    alertDismissed, // callback
                    '', // title
                    'Aceptar'                  // buttonName
                    );
        } else {
            alert('No existen registros.' + txt);
        }
    }
}
//funcion button para sincronizar
jQuery('#btn_sync').on('click', function (evt) {

    conta_sync = 0;
    percentaje_txt = 0;
    app.eliminarTablas();
    app.crearTablas();
    app.sync_all();
    //LLenando tablas
//    app.consumeClientes();
//    app.consumeProductos();
//    app.consumePedidos();
//    app.consumeFormaPago();
//    app.consumeDireccionCliente();
//    app.consumeBancos();
//    app.consumeMotivoNoVenta();
//    app.consumeAgencias();
//    app.consumeDescuentos();
//    app.consumeDetalleListaPrecios();


});
app.sync_all = function () {

    loadingMsg("Descargando datos ...");
    console.log('consumiendo todos los WS');
    var sql_st = 'insert into clientes (IdCliente,Codigo,Descripcion,CodCategoria,Ruc,Dni,LimiteCredito,LimiteCreditoDisp,CodFormaPago,CodVendedor,FlgRegHabilitado,CodAgencia,CodTipoNegocio,DescripcionNegocio,Agencia,Correo,Telefono)  values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    var arr_cl = [];
    var user = getCurrentUser();
    if (user === false) {
        if (produccion) {
            navigator.notification.alert(
                    'No existe usuario logueado.', // message
                    alertDismissed, // callback
                    'Mensaje del Sistema!', // title
                    'Aceptar'                  // buttonName
                    );
        } else {
            alert('No existe usuario logueado.');
        }
        return;
    }

    var fecha_ini = '';
    var fecha_fin = '';
    var obj_data = {
        vendedor_id: user,
        fecha_ini: fecha_ini,
        fecha_fin: fecha_fin,
    };

    jQuery.ajaxSetup({
        cache: false,
        async: true,
    });
    jQuery.ajax({
        url: config.rest_url + '/ws_sync',
        type: 'jsonp',
        method: 'GET',
        data: obj_data,
        xhrFields: {
            withCredentials: false
        },
        headers: {
        },
        success: function (xhr) {


            app.db.transaction(function (tx) {
                loadingMsg("Cargando clientes...");
                for (var i = 0; i < xhr.clientes.length; i++) {
                    arr_cl = [
                        xhr.clientes[i]["IdCliente"],
                        xhr.clientes[i]["Codigo"],
                        xhr.clientes[i]["Descripcion"],
                        xhr.clientes[i]["CodCategoria"],
                        xhr.clientes[i]["Ruc"],
                        xhr.clientes[i]["Dni"],
                        xhr.clientes[i]["LimiteCredito"],
                        xhr.clientes[i]["LimiteCreditoDisp"],
                        xhr.clientes[i]["CodFormaPago"],
                        xhr.clientes[i]["CodVendedor"],
                        xhr.clientes[i]["FlgRegHabilitado"],
                        xhr.clientes[i]["CodAgencia"],
                        xhr.clientes[i]["CodTipoNegocio"],
                        xhr.clientes[i]["DescripcionNegocio"],
                        xhr.clientes[i]["Agencia"],
                        xhr.clientes[i]["Correo"],
                        xhr.clientes[i]["Telefono"],
                    ];
                    tx.executeSql(sql_st, arr_cl, app.onSuccess, app.onError);
                }
                hideLoading("clientes", true);
            });
            var txtFechaClientes = app.obtenerFechaHora();
            var sql_clientes_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
            var arr_clientes_sync = [user, txtFechaClientes, 'Clientes'];
            app.db.transaction(function (tx) {
                tx.executeSql(sql_clientes_sync, arr_clientes_sync, app.onSuccess, app.onError);
            });

            // Insertamos los productos

            var sql_stp = 'insert into producto (IdProducto,Codigo,Descripcion,Presentacion,Stock,CodCategoria,FlgRegHabilitado,Categoria)  values (?,?,?,?,?,?,?,?)';
            var arr_prd = [];
            app.db.transaction(function (tx) {
                loadingMsg("Cargando productos...");
                for (var i = 0; i < xhr.productos.length; i++) {
                    arr_prd = [
                        xhr.productos[i]["IdProducto"],
                        xhr.productos[i]["Codigo"],
                        xhr.productos[i]["Descripcion"],
                        xhr.productos[i]["Presentacion"],
                        xhr.productos[i]["Stock"],
                        xhr.productos[i]["CodCategoria"],
                        xhr.productos[i]["FlgRegHabilitado"],
                        xhr.productos[i]["Categoria"],
                    ];
                    tx.executeSql(sql_stp, arr_prd, app.onSuccess, app.onError);

                }
                hideLoading("productos", true);
            });
            var txtFechaProductos = app.obtenerFechaHora();
            var sql_producto_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
            var arr_producto_sync = [user, txtFechaProductos, 'Productos'];
            app.db.transaction(function (tx) {
                tx.executeSql(sql_producto_sync, arr_producto_sync, app.onSuccess, app.onError);
            });

            // Insertamos los Forma Pago
            var sql_stped1 = 'insert into FormaPago (IdFormaPago,' +
                    'Codigo,' +
                    'Descripcion,' +
                    'GrupoForPago,' +
                    'FlgDescuento3,' +
                    'FlgRegHabilitado' +
                    ') values (?,?,?,?,?,?)';
            var arr_formaPago1 = [];
            app.db.transaction(function (tx) {
                loadingMsg("Cargando formas de pago...");
                for (var i = 0; i < xhr.forma_pago.length; i++) {
                    arr_formaPago1 = [
                        xhr.forma_pago[i]["IdFormaPago"],
                        xhr.forma_pago[i]["Codigo"],
                        xhr.forma_pago[i]["Descripcion"],
                        xhr.forma_pago[i]["GrupoForPago"],
                        xhr.forma_pago[i]["FlgDescuento3"],
                        xhr.forma_pago[i]["FlgRegHabilitado"],
                    ];

                    tx.executeSql(sql_stped1, arr_formaPago1, app.onSuccess, app.onError);
                }
                hideLoading("FormaPago", true);
            });

            var sql_direccionCliente = 'insert into Direccioncliente (Codigo,Descripcion,TipoDireccion) values (?,?,?)';
            var arr_direccionCliente = [];

            app.db.transaction(function (tx) {
                loadingMsg("Cargando direcciones...");
                for (var i = 0; i < xhr.direccion.length; i++) {
                    arr_direccionCliente = [
                        xhr.direccion[i]["Codigo"],
                        xhr.direccion[i]["Descripcion"],
                        xhr.direccion[i]["TipoDireccion"],
                    ];

                    tx.executeSql(sql_direccionCliente, arr_direccionCliente, app.onSuccess, app.onError);
                }
                hideLoading("direcciones", true);
            });

            var txtFechaPedidos = app.obtenerFechaHora();
            var sql_pedidos_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
            var arr_pedidos_sync = [user, txtFechaPedidos, 'Dirección clientes'];
            app.db.transaction(function (tx) {
                tx.executeSql(sql_pedidos_sync, arr_pedidos_sync, app.onSuccess, app.onError);
            });

            // Bancos

            var sql_stbancos = 'insert into Bancos (Codigo,NombreLargo,NombreCorto,FlgRegHabilitado) values (?,?,?,?)';
            var arr_bancos = [];

            app.db.transaction(function (tx) {
                loadingMsg("Cargando bancos...");
                for (var i = 0; i < xhr.bancos.length; i++) {
                    arr_bancos = [
                        xhr.bancos[i]["Codigo"],
                        xhr.bancos[i]["NombreLargo"],
                        xhr.bancos[i]["NombreCorto"],
                        xhr.bancos[i]["FlgRegHabilitado"],
                    ];

                    tx.executeSql(sql_stbancos, arr_bancos, app.onSuccess, app.onError);
                }
                hideLoading("bancos", true);
            });

            var txtFechaPedidos = app.obtenerFechaHora();
            var sql_pedidos_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
            var arr_pedidos_sync = [user, txtFechaPedidos, 'Bancos'];
            app.db.transaction(function (tx) {
                tx.executeSql(sql_pedidos_sync, arr_pedidos_sync, app.onSuccess, app.onError);
            });

            // No venta

            var sql_noventa = 'insert into MotivoNoVenta (IdMotivoNoPedido,Codigo,Descripcion,abreviatura,FlgRegHabilitado) values (?,?,?,?,?)';
            var arr_noventa = [];
            app.db.transaction(function (tx) {
                loadingMsg("Cargando motivos de no venta...");
                for (var i = 0; i < xhr.motivo_no_pedido.length; i++) {
                    arr_noventa = [
                        xhr.motivo_no_pedido[i]["IdMotivoNoPedido"],
                        xhr.motivo_no_pedido[i]["Codigo"],
                        xhr.motivo_no_pedido[i]["Descripcion"],
                        xhr.motivo_no_pedido[i]["abreviatura"],
                        xhr.motivo_no_pedido[i]["FlgRegHabilitado"],
                    ];

                    tx.executeSql(sql_noventa, arr_noventa, app.onSuccess, app.onError);
                }
                hideLoading("MotivoNoVenta", true);
            });
            var txtFechaPedidos = app.obtenerFechaHora();
            var sql_pedidos_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
            var arr_pedidos_sync = [user, txtFechaPedidos, 'Motivos no venta'];
            app.db.transaction(function (tx) {
                tx.executeSql(sql_pedidos_sync, arr_pedidos_sync, app.onSuccess, app.onError);
            });

            // Agencias
            var sql_agencias = 'insert into Agencias (IdAgencia,Codigo,Descripcion,FlgRegHabilitado) values (?,?,?,?)';
            var arr_agencias = [];

            app.db.transaction(function (tx) {
                loadingMsg("Cargando agencias...");
                for (var i = 0; i < xhr.agencias.length; i++) {
                    arr_agencias = [
                        xhr.agencias[i]["IdAgencia"],
                        xhr.agencias[i]["Codigo"],
                        xhr.agencias[i]["Descripcion"],
                        xhr.agencias[i]["FlgRegHabilitado"],
                    ];

                    tx.executeSql(sql_agencias, arr_agencias, app.onSuccess, app.onError);
                }
                hideLoading("Agencias", true);
            });
            var txtFechaPedidos = app.obtenerFechaHora();
            var sql_pedidos_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
            var arr_pedidos_sync = [user, txtFechaPedidos, 'Agencias'];
            app.db.transaction(function (tx) {
                tx.executeSql(sql_pedidos_sync, arr_pedidos_sync, app.onSuccess, app.onError);
            });

// Descuentos
            var sql_agencias2 = 'insert into Descuentos (Tipo,Lista,Valor,Min_m,Max_m) values (?,?,?,?,?)';
            var arr_agencias2 = [];

            app.db.transaction(function (tx) {
                loadingMsg("Cargando descuentos...");
                for (var i = 0; i < xhr.descuentos.length; i++) {
                    arr_agencias2 = [
                        xhr.descuentos[i]["Tipo"],
                        xhr.descuentos[i]["Lista"],
                        xhr.descuentos[i]["Valor"],
                        xhr.descuentos[i]["Min_m"],
                        xhr.descuentos[i]["Max_m"],
                    ];

                    tx.executeSql(sql_agencias2, arr_agencias2, app.onSuccess, app.onError);

                }
                hideLoading("descuentos", true);
            });
            var txtFechaPedidos = app.obtenerFechaHora();
            var sql_pedidos_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
            var arr_pedidos_sync = [user, txtFechaPedidos, 'Descuentos'];
            app.db.transaction(function (tx) {
                tx.executeSql(sql_pedidos_sync, arr_pedidos_sync, app.onSuccess, app.onError);
            });
            //

            var sql_detallelistaprecios = 'insert into DetalleListaPrecios (IdDetalleListaPrecio,CodigoItem,CodigoUM,CodigoLPrecio,PrecioVenta,PrecioOferta,Descripcion,Commoditie1,Commoditie2) values (?,?,?,?,?,?,?,?,?)';
            var arr_detallelistaprecios = [];

            app.db.transaction(function (tx) {
                loadingMsg("Cargando lista de precios...");
                for (var i = 0; i < xhr.detalle_lista_precios.length; i++) {
                    arr_detallelistaprecios = [
                        xhr.detalle_lista_precios[i]["IdDetalleListaPrecio"],
                        xhr.detalle_lista_precios[i]["CodigoItem"],
                        xhr.detalle_lista_precios[i]["CodigoUM"],
                        xhr.detalle_lista_precios[i]["CodigoLPrecio"],
                        xhr.detalle_lista_precios[i]["PrecioVenta"],
                        xhr.detalle_lista_precios[i]["PrecioOferta"],
                        xhr.detalle_lista_precios[i]["Descripcion"],
                        xhr.detalle_lista_precios[i]["Commoditie1"],
                        xhr.detalle_lista_precios[i]["Commoditie2"],
                    ];

                    tx.executeSql(sql_detallelistaprecios, arr_detallelistaprecios, app.onSuccess, app.onError);
                }
                hideLoading("DetalleListaPrecios", true);
            });
            var txtFechaDetListaprecios = app.obtenerFechaHora();
            var sql_detLista_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
            var arr_detLista_sync = [user, txtFechaDetListaprecios, 'DetalleListaprecios'];
            app.db.transaction(function (tx) {
                tx.executeSql(sql_detLista_sync, arr_detLista_sync, app.onSuccess, app.onError);
            });



//                hideLoading("consumeClientes", true);
//                
//            } else {
//                hideLoading("consumeClientes", false);
//            }
        }
    });
}
//Método de consumo de servicio tabla Clientes
app.consumeClientes = function () {

    console.log('consumiendo ws cliente');
    var sql_st = 'insert into clientes (IdCliente,Codigo,Descripcion,CodCategoria,Ruc,Dni,LimiteCredito,LimiteCreditoDisp,CodFormaPago,CodVendedor,FlgRegHabilitado,CodAgencia,CodTipoNegocio,DescripcionNegocio,Agencia,Correo,Telefono)  values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    var arr_cl = [];
    var user = getCurrentUser();
    if (user === false) {
        if (produccion) {
            navigator.notification.alert(
                    'No existe usuario logueado.', // message
                    alertDismissed, // callback
                    'Mensaje del Sistema!', // title
                    'Aceptar'                  // buttonName
                    );
        } else {
            alert('No existe usuario logueado.');
        }
        return;
    }

    jQuery.ajaxSetup({
        cache: false,
        async: true,
    });
    jQuery.ajax({
        url: config.rest_url + '/ws_cliente/' + user,
        type: 'jsonp',
        method: 'GET',
        xhrFields: {
            withCredentials: false
        },
        headers: {
        },
        success: function (xhr) {
            if (xhr.length > 0) {

//                app.db.transaction(function (tx) {
//                    loadingMsg("Cargando clientes...");
//                    for (var i = 0; i < xhr.length; i++) {
//                        arr_cl = [
//                            xhr[i]["IdCliente"],
//                            xhr[i]["Codigo"],
//                            xhr[i]["Descripcion"],
//                            xhr[i]["CodCategoria"],
//                            xhr[i]["Ruc"],
//                            xhr[i]["Dni"],
//                            xhr[i]["LimiteCredito"],
//                            xhr[i]["LimiteCreditoDisp"],
//                            xhr[i]["CodFormaPago"],
//                            xhr[i]["CodVendedor"],
//                            xhr[i]["FlgRegHabilitado"],
//                            xhr[i]["CodAgencia"],
//                            xhr[i]["CodTipoNegocio"],
//                            xhr[i]["DescripcionNegocio"],
//                            xhr[i]["Agencia"],
//                            xhr[i]["Correo"],
//                            xhr[i]["Telefono"],
//                        ];
//                        tx.executeSql(sql_st, arr_cl, app.onSuccess, app.onError);
//                    }
//                });
//                var txtFechaClientes = app.obtenerFechaHora();
//                var sql_clientes_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
//                var arr_clientes_sync = [user, txtFechaClientes, 'Clientes'];
//                app.db.transaction(function (tx) {
//                    tx.executeSql(sql_clientes_sync, arr_clientes_sync, app.onSuccess, app.onError);
//                });
//                hideLoading("consumeClientes", true);

            } else {
                hideLoading("consumeClientes", false);
            }
        }
    });
}
//Método de consumo de servicio tabla Productos
app.consumeProductos = function () {
    console.log('consumiendo ws producto');
//    var sql_stp = 'insert into producto (IdProducto,Codigo,Descripcion,Presentacion,Stock,CodCategoria,FlgRegHabilitado,Categoria , PrecioVenta,CodigoUM,CodigoLPrecio )  values (?,?,?,?,?,?,?,?,?,?,?)';
    var sql_stp = 'insert into producto (IdProducto,Codigo,Descripcion,Presentacion,Stock,CodCategoria,FlgRegHabilitado,Categoria)  values (?,?,?,?,?,?,?,?)';
    var arr_prd = [];
    var user = getCurrentUser();
    if (user === false) {
        navigator.notification.alert(
                'No existe usuario logueado.', // message
                alertDismissed, // callback
                'Mensaje del Sistema', // title
                'Aceptar'                  // buttonName
                );
        return;
    }
    jQuery.ajaxSetup({
        cache: false,
        async: true,
    })
    jQuery.ajax({
        url: config.rest_url + '/ws_productos',
        type: 'jsonp',
        method: 'GET',
        xhrFields: {
            withCredentials: false
        },
        headers: {
        },
        success: function (xhr) {
            if (xhr.length > 0) {
                app.db.transaction(function (tx) {
                    loadingMsg("Cargando productos...");
                    for (var i = 0; i < xhr.length; i++) {
                        arr_prd = [
                            xhr[i]["IdProducto"],
                            xhr[i]["Codigo"],
                            xhr[i]["Descripcion"],
                            xhr[i]["Presentacion"],
                            xhr[i]["Stock"],
                            xhr[i]["CodCategoria"],
                            xhr[i]["FlgRegHabilitado"],
                            xhr[i]["Categoria"],
                        ];
                        tx.executeSql(sql_stp, arr_prd, app.onSuccess, app.onError);

                    }
                });
                var txtFechaProductos = app.obtenerFechaHora();
                var sql_producto_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
                var arr_producto_sync = [user, txtFechaProductos, 'Productos'];
                app.db.transaction(function (tx) {
                    tx.executeSql(sql_producto_sync, arr_producto_sync, app.onSuccess, app.onError);
                });
                hideLoading("consumeProductos", true);
                $.mobile.loading('hide');
            } else {
                hideLoading("consumeProductos", false);
            }
        }
    });
}
//Método de consumo de servicio tabla Productos
app.consumePedidos = function () {
    hideLoading("consumePedidos", true);
//    console.log('consumiendo ws pedidos server');
//
//    var user = getCurrentUser();
//    if (user === false) {
//        navigator.notification.alert(
//                'No existe usuario logueado.', // message
//                alertDismissed, // callback
//                'Mensaje del Sistema', // title
//                'Aceptar'                  // buttonName
//                );
//        return;
//    }
//    var sql_stped = 'insert into CabeceraPedidoServer (IdPedido,' +
//            'CodVendedor,' +
//            'CodCanalVenta,' +
//            'CodCliente,' +
//            'CodTipoDoc,' +
//            'CodFormaPago,' +
//            'CodMoneda,' +
//            'Letras,' +
//            'ObsLetras,' +
//            'CodAgencia,' +
//            'DirEntrega,' +
//            'DirFacturacion,' +
//            'FechaEntrega,' +
//            'Observaciones,' +
//            'MntSubTotal,' +
//            'MntIGV,' +
//            'MntTotal,' +
//            'CodMotivoNoPedido,' +
//            'FechaInicio,' +
//            'FechaFin,' +
//            'FechaDB,' +
//            'FlgMigracion,' +
//            'MntBruto,' +
//            'MntDscto,' +
//            'Latitud,' +
//            'Longitud) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
//    var arr_ped = [];
//
//    var user = getCurrentUser();
//    if (user === false) {
//        navigator.notification.alert(
//                'No existe usuario logueado.', // message
//                alertDismissed, // callback
//                'Mensaje del Sistema', // title
//                'Aceptar'                  // buttonName
//                );
//        return;
//    }
//    jQuery.ajaxSetup({
//        cache: false,
//        async: true,
//    });
//    jQuery.ajax({
//        url: config.rest_url + '/ws_pedidos?vendedor_id=' + user,
//        type: 'jsonp',
//        method: 'GET',
//        xhrFields: {
//            withCredentials: false
//        },
//        headers: {
//        },
//        success: function (xhr) {
//            if (xhr.length > 0) {
//                app.db.transaction(function (tx) {
//
//                    for (var i = 0; i < xhr.length; i++) {
//                        arr_ped = [
//                            xhr[i]["IdPedido"],
//                            xhr[i]["CodVendedor"],
//                            xhr[i]["CodCanalVenta"],
//                            xhr[i]["CodCliente"],
//                            xhr[i]["CodTipoDoc"],
//                            xhr[i]["CodFormaPago"],
//                            xhr[i]["CodMoneda"],
//                            xhr[i]["Letras"],
//                            xhr[i]["ObsLetras"],
//                            xhr[i]["CodAgencia"],
//                            xhr[i]["DirEntrega"],
//                            xhr[i]["DirFacturacion"],
//                            xhr[i]["FechaEntrega"],
//                            xhr[i]["Observaciones"],
//                            xhr[i]["MntSubTotal"],
//                            xhr[i]["MntIGV"],
//                            xhr[i]["MntTotal"],
//                            xhr[i]["CodMotivoNoPedido"],
//                            xhr[i]["FechaInicio"],
//                            xhr[i]["FechaFin"],
//                            xhr[i]["FechaDB"],
//                            xhr[i]["FlgMigracion"],
//                            xhr[i]["MntBruto"],
//                            xhr[i]["MntDscto"],
//                            xhr[i]["Latitud"],
//                            xhr[i]["Longitud"]
//                        ];
//
//                        tx.executeSql(sql_stped, arr_ped, app.onSuccess, app.onError);
//
//                    }
//                });
//                var txtFechaPedidos = app.obtenerFechaHora();
//                var sql_pedidos_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
//                var arr_pedidos_sync = [user, txtFechaPedidos, 'Pedidos'];
//                app.db.transaction(function (tx) {
//                    tx.executeSql(sql_pedidos_sync, arr_pedidos_sync, app.onSuccess, app.onError);
//                });
//                hideLoading("consumePedidos", true);
//            } else {
//                hideLoading("consumePedidos", false);
//            }
//        }
//    });
}
//Método de consumo de servicio tabla FormaPago
app.consumeFormaPago = function () {
    console.log('consumiendo ws Forma Pago');
    var sql_stped = 'insert into FormaPago (IdFormaPago,' +
            'Codigo,' +
            'Descripcion,' +
            'GrupoForPago,' +
            'FlgDescuento3,' +
            'FlgRegHabilitado' +
            ') values (?,?,?,?,?,?)';
    var arr_formaPago = [];

    var user = getCurrentUser();
    if (user === false) {
//        alert('No existe usuario logueado.');
        return;
    }
    jQuery.ajaxSetup({
        cache: false,
        async: true,
    });
    jQuery.ajax({
        url: config.rest_url + '/ws_formapago',
        type: 'jsonp',
        method: 'GET',
        xhrFields: {
            withCredentials: false
        },
        headers: {
        },
        success: function (xhr) {
            if (xhr.length > 0) {
                app.db.transaction(function (tx) {
                    loadingMsg("Cargando formas de pago...");
                    for (var i = 0; i < xhr.length; i++) {
                        arr_formaPago = [
                            xhr[i]["IdFormaPago"],
                            xhr[i]["Codigo"],
                            xhr[i]["Descripcion"],
                            xhr[i]["GrupoForPago"],
                            xhr[i]["FlgDescuento3"],
                            xhr[i]["FlgRegHabilitado"],
                        ];

                        tx.executeSql(sql_stped, arr_formaPago, app.onSuccess, app.onError);
                    }
                });
                hideLoading("consumeFormaPago", true);
            } else {
                hideLoading("consumeFormaPago", false);
            }
        }
    });
}
//Metodo para obtener consumeDireccionCliente 
app.consumeDireccionCliente = function () {
    console.log('consumiendo ws Direccion Cliente');
    var sql_stped = 'insert into Direccioncliente (Codigo,Descripcion,TipoDireccion) values (?,?,?)';
    var arr_formaPago = [];

    var user = getCurrentUser();
    if (user === false) {
//        alert('No existe usuario logueado.');
        return;
    }
    jQuery.ajaxSetup({
        cache: false,
        async: true,
    });

    jQuery.ajax({
        url: config.rest_url + '/ws_direccion_cliente?vendedor_id=' + user,
        type: 'jsonp',
        method: 'GET',
        xhrFields: {
            withCredentials: false
        },
        headers: {
        },
        success: function (xhr) {
            if (xhr.length > 0) {
                app.db.transaction(function (tx) {
                    loadingMsg("Cargando direcciones...");
                    for (var i = 0; i < xhr.length; i++) {
                        arr_formaPago = [
                            xhr[i]["Codigo"],
                            xhr[i]["Descripcion"],
                            xhr[i]["TipoDireccion"],
                        ];

                        tx.executeSql(sql_stped, arr_formaPago, app.onSuccess, app.onError);
                    }
                });

                var txtFechaPedidos = app.obtenerFechaHora();
                var sql_pedidos_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
                var arr_pedidos_sync = [user, txtFechaPedidos, 'Dirección clientes'];
                app.db.transaction(function (tx) {
                    tx.executeSql(sql_pedidos_sync, arr_pedidos_sync, app.onSuccess, app.onError);
                });
                hideLoading("consumeDireccionCliente", true);
            } else {
                hideLoading("consumeDireccionCliente", false);
            }
        }
    });


}
//consume bancos
app.consumeBancos = function () {
    console.log('consumiendo ws Bancos');
    var sql_stbancos = 'insert into Bancos (Codigo,NombreLargo,NombreCorto,FlgRegHabilitado) values (?,?,?,?)';
    var arr_bancos = [];

    var user = getCurrentUser();
    if (user === false) {
//        alert('No existe usuario logueado.');
        return;
    }
    jQuery.ajaxSetup({
        cache: false,
        async: true,
    });


    jQuery.ajax({
        url: config.rest_url + '/ws_bancos',
        type: 'jsonp',
        method: 'GET',
        xhrFields: {
            withCredentials: false
        },
        headers: {
        },
        success: function (xhr) {
            if (xhr.length > 0) {
                app.db.transaction(function (tx) {
                    loadingMsg("Cargando bancos...");
                    for (var i = 0; i < xhr.length; i++) {
                        arr_bancos = [
                            xhr[i]["Codigo"],
                            xhr[i]["NombreLargo"],
                            xhr[i]["NombreCorto"],
                            xhr[i]["FlgRegHabilitado"],
                        ];

                        tx.executeSql(sql_stbancos, arr_bancos, app.onSuccess, app.onError);
                    }
                });

                var txtFechaPedidos = app.obtenerFechaHora();
                var sql_pedidos_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
                var arr_pedidos_sync = [user, txtFechaPedidos, 'Bancos'];
                app.db.transaction(function (tx) {
                    tx.executeSql(sql_pedidos_sync, arr_pedidos_sync, app.onSuccess, app.onError);
                });
                hideLoading("consumeBancos", true);
            } else {
                hideLoading("consumeBancos", false);
            }
        }
    });


}
//Método para obtener consumeMotivoNoVenta
app.consumeMotivoNoVenta = function () {
    console.log('consumiendo ws consumeMotivoNoVenta');
    var sql_noventa = 'insert into MotivoNoVenta (IdMotivoNoPedido,Codigo,Descripcion,abreviatura,FlgRegHabilitado) values (?,?,?,?,?)';
    var arr_noventa = [];

    var user = getCurrentUser();
    if (user === false) {
//        alert('No existe usuario logueado.');
        return;
    }
    jQuery.ajaxSetup({
        cache: false,
        async: true,
    });


    jQuery.ajax({
        url: config.rest_url + '/ws_motivo_no_pedido',
        type: 'jsonp',
        method: 'GET',
        xhrFields: {
            withCredentials: false
        },
        headers: {
        },
        success: function (xhr) {
            if (xhr.length > 0) {
                app.db.transaction(function (tx) {
                    loadingMsg("Cargando motivos de no venta...");
                    for (var i = 0; i < xhr.length; i++) {
                        arr_noventa = [
                            xhr[i]["IdMotivoNoPedido"],
                            xhr[i]["Codigo"],
                            xhr[i]["Descripcion"],
                            xhr[i]["abreviatura"],
                            xhr[i]["FlgRegHabilitado"],
                        ];

                        tx.executeSql(sql_noventa, arr_noventa, app.onSuccess, app.onError);
                    }
                });
                var txtFechaPedidos = app.obtenerFechaHora();
                var sql_pedidos_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
                var arr_pedidos_sync = [user, txtFechaPedidos, 'Motivos no venta'];
                app.db.transaction(function (tx) {
                    tx.executeSql(sql_pedidos_sync, arr_pedidos_sync, app.onSuccess, app.onError);
                });

                hideLoading("consumeMotivoNoVenta", true);
            } else {
                hideLoading("consumeMotivoNoVenta", false);
            }
        }
    });

}
//Consume ws agencias
app.consumeAgencias = function () {
    console.log('consumiendo ws Agencias');
    var sql_agencias = 'insert into Agencias (IdAgencia,Codigo,Descripcion,FlgRegHabilitado) values (?,?,?,?)';
    var arr_agencias = [];

    var user = getCurrentUser();
    if (user === false) {
//        alert('No existe usuario logueado.');
        return;
    }
    jQuery.ajaxSetup({
        cache: false,
        async: true,
    });


    jQuery.ajax({
        url: config.rest_url + '/ws_agencias',
        type: 'jsonp',
        method: 'GET',
        xhrFields: {
            withCredentials: false
        },
        headers: {
        },
        success: function (xhr) {
            if (xhr.length > 0) {
                app.db.transaction(function (tx) {
                    loadingMsg("Cargando agencias...");
                    for (var i = 0; i < xhr.length; i++) {
                        arr_agencias = [
                            xhr[i]["IdAgencia"],
                            xhr[i]["Codigo"],
                            xhr[i]["Descripcion"],
                            xhr[i]["FlgRegHabilitado"],
                        ];

                        tx.executeSql(sql_agencias, arr_agencias, app.onSuccess, app.onError);
                    }
                });
                var txtFechaPedidos = app.obtenerFechaHora();
                var sql_pedidos_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
                var arr_pedidos_sync = [user, txtFechaPedidos, 'Agencias'];
                app.db.transaction(function (tx) {
                    tx.executeSql(sql_pedidos_sync, arr_pedidos_sync, app.onSuccess, app.onError);
                });
                hideLoading("consumeAgencias", true);

            } else {
                hideLoading("consumeAgencias", false);
            }
        }
    });

}
//Consume ws descuentos
app.consumeDescuentos = function () {
    console.log('consumiendo ws Descuentos');
    var sql_agencias = 'insert into Descuentos (Tipo,Lista,Valor,Min_m,Max_m) values (?,?,?,?,?)';
    var arr_agencias = [];

    var user = getCurrentUser();
    if (user === false) {
//        alert('No existe usuario logueado.');
        return;
    }
    jQuery.ajaxSetup({
        cache: false,
        async: true,
    });

    jQuery.ajax({
        url: config.rest_url + '/ws_descuentos',
        type: 'jsonp',
        method: 'GET',
        xhrFields: {
            withCredentials: false
        },
        headers: {
        },
        success: function (xhr) {
            if (xhr.length > 0) {
                app.db.transaction(function (tx) {
                    loadingMsg("Cargando descuentos...");
                    for (var i = 0; i < xhr.length; i++) {
                        arr_agencias = [
                            xhr[i]["Tipo"],
                            xhr[i]["Lista"],
                            xhr[i]["Valor"],
                            xhr[i]["Min_m"],
                            xhr[i]["Max_m"],
                        ];

                        tx.executeSql(sql_agencias, arr_agencias, app.onSuccess, app.onError);
                    }
                });
                var txtFechaPedidos = app.obtenerFechaHora();
                var sql_pedidos_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
                var arr_pedidos_sync = [user, txtFechaPedidos, 'Descuentos'];
                app.db.transaction(function (tx) {
                    tx.executeSql(sql_pedidos_sync, arr_pedidos_sync, app.onSuccess, app.onError);
                });
                hideLoading("consumeDescuentos", true);


            } else {
                hideLoading("consumeDescuentos", false);
            }
        }
    });


}
//Consume ws Lista de precios
app.consumeListaPrecios = function () {
    console.log('consumiendo ws ListaPrecios');
    var sql_listaprecios = 'insert into ListaPrecios (IdListaPrecio,Codigo,Descripcion,CodigoMoneda,FlgDsctoTipoVe,FlgRegHabilitado) values (?,?,?,?,?,?)';
    var arr_listaprecios = [];

    var user = getCurrentUser();
    if (user === false) {
        return;
    }
    jQuery.ajaxSetup({
        cache: false,
        async: true,
    });

    jQuery.ajax({
        url: config.rest_url + '/ws_descuentos',
        type: 'jsonp',
        method: 'GET',
        xhrFields: {
            withCredentials: false
        },
        headers: {
        },
        success: function (xhr) {
            if (xhr.length > 0) {
                app.db.transaction(function (tx) {
                    for (var i = 0; i < xhr.length; i++) {
                        arr_listaprecios = [
                            xhr[i]["IdListaPrecio"],
                            xhr[i]["Codigo"],
                            xhr[i]["Descripcion"],
                            xhr[i]["CodigoMoneda"],
                            xhr[i]["FlgDsctoTipoVe"],
                            xhr[i]["FlgRegHabilitado"],
                        ];

                        tx.executeSql(sql_listaprecios, arr_listaprecios, app.onSuccess, app.onError);
                    }
                    hideLoading("consumeDetalleListaPrecios", true);
                });

                var txtFechaListaprecios = app.obtenerFechaHora();
                var sql_listaprecios_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
                var arr_listaprecios_sync = [user, txtFechaListaprecios, 'Listaprecios'];
                app.db.transaction(function (tx) {
                    tx.executeSql(sql_listaprecios_sync, arr_listaprecios_sync, app.onSuccess, app.onError);
                });
            } else {
                hideLoading("consumeDetalleListaPrecios", false);
            }
        }
    });

}
//Consume detalle lista precios
app.consumeDetalleListaPrecios = function () {
    console.log('consumiendo ws DetalleListaPrecios');

    var sql_detallelistaprecios = 'insert into DetalleListaPrecios (IdDetalleListaPrecio,CodigoItem,CodigoUM,CodigoLPrecio,PrecioVenta,PrecioOferta,Descripcion,Commoditie1,Commoditie2) values (?,?,?,?,?,?,?,?,?)';
    var arr_detallelistaprecios = [];

    var user = getCurrentUser();
    if (user === false) {
        return;
    }
    jQuery.ajaxSetup({
        cache: false,
        async: true,
    });

    jQuery.ajax({
        url: config.rest_url + '/ws_detlistaprecios',
        type: 'jsonp',
        method: 'GET',
        xhrFields: {
            withCredentials: false
        },
        headers: {
        },
        success: function (xhr) {
            if (xhr.length > 0) {
                app.db.transaction(function (tx) {
                    loadingMsg("Cargando lista de precios...");
                    for (var i = 0; i < xhr.length; i++) {
                        arr_detallelistaprecios = [
                            xhr[i]["IdDetalleListaPrecio"],
                            xhr[i]["CodigoItem"],
                            xhr[i]["CodigoUM"],
                            xhr[i]["CodigoLPrecio"],
                            xhr[i]["PrecioVenta"],
                            xhr[i]["PrecioOferta"],
                            xhr[i]["Descripcion"],
                            xhr[i]["Commoditie1"],
                            xhr[i]["Commoditie2"],
                        ];

                        tx.executeSql(sql_detallelistaprecios, arr_detallelistaprecios, app.onSuccess, app.onError);
                    }
                    hideLoading("consumeDetalleListaPrecios", true);
                });
                var txtFechaDetListaprecios = app.obtenerFechaHora();
                var sql_detLista_sync = 'insert into log_sync (CodVendedor,fecha_sync,tipo)  values (?,?,?)';
                var arr_detLista_sync = [user, txtFechaDetListaprecios, 'DetalleListaprecios'];
                app.db.transaction(function (tx) {
                    tx.executeSql(sql_detLista_sync, arr_detLista_sync, app.onSuccess, app.onError);
                });

            } else {
                hideLoading("consumeDetalleListaPrecios", false);
            }
        }
    });


}
//Método para obtener Fecha y hora de sincronización
app.obtenerFechaHora = function () {
    var fecha_yxy = new Date();
    var dias = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sabado");
    var mes = fecha_yxy.getMonth() + 1;
    var fecha_txto = 'Actualizado al ' + fecha_yxy.getDate() + '/' + mes + '/' + fecha_yxy.getYear() + ' a las ' + fecha_yxy.getHours() + ':' + fecha_yxy.getMinutes() + ':' + fecha_yxy.getSeconds();

    return fecha_txto;
}
