$(document).on('pagebeforeshow', '#modulo_editar_cliente', function () {
    app.db.transaction(listarEditarCliente, app.onError);
});

function listarEditarCliente(tx) {

    tx.executeSql("Select correo,telefono from clientes where codigo=?", [dataCliente.id], function (tx, result) {
        var ncli = result.rows.length;
        $('#txt_edit_cliente_correo').val('');
        $('#txt_edit_cliente_telefono').val('');

        if (ncli > 0) {
            var row = result.rows.item(0);
            $('#txt_edit_cliente_correo').val(row['Correo']);
            $('#txt_edit_cliente_telefono').val(row['Telefono']);
        } else {
            console.log("no hay datos forma de pago");
        }
    });
}

function updateCliente(correo, telefono, codigo) {
    app.db.transaction(function (tx) {
        tx.executeSql('update clientes set Correo=?,Telefono=? where codigo=?', [correo, telefono, codigo], app.onSuccess, app.onError);
    });
}


$(document).on('vclick', '#btn_editar_cliente', function () {

    var correo = $('#txt_edit_cliente_correo').val();
    var telefono = $('#txt_edit_cliente_telefono').val();
    if (correo.length === 0) {

        if (produccion) {
            navigator.notification.alert(
                    'Debe ingresar un correo.', // message
                    alertDismissed, // callback
                    '', // title
                    'Aceptar'                  // buttonName
                    );
        } else {
            alert('Debe ingresar un correo.');
        }
        return;
    }
    if (!validarEmail(correo)) {

        if (produccion) {
            navigator.notification.alert(
                    'El correo ingresado no tiene un formato válido.', // message
                    alertDismissed, // callback
                    '', // title
                    'Aceptar'                  // buttonName
                    );
        } else {
            alert('El correo ingresado no tiene un formato válido.');
        }
        return;
    }

    updateCliente(correo, telefono, dataCliente.id);

    var obj_edit_cliente = {
        correo: correo,
        telefono: telefono,
    };
    jQuery.ajaxSetup({
        contentType: "application/json; charset=utf-8",
        async: false,
        cache: false
    });
    jQuery.ajax({
        url: config.rest_url + '/ws_cliente/' + dataCliente.id,
        data: JSON.stringify(obj_edit_cliente),
        method: 'PUT',
        type: 'json',
        success: function (xhr) {
            $("#btn_guaradar_pedido").prop("disabled", false);
            if (parseInt(xhr.id) === 100) {
                if (produccion) {
                    navigator.notification.alert(
                            'Los datos fueron actualizados correctamente.', // message
                            alertDismissed, // callback
                            '', // title
                            'Aceptar'                  // buttonName
                            );
                } else {
                    alert('Los datos fueron actualizados correctamente.');
                }
                $.mobile.changePage("#detalle_cliente");
            } else {
                if (produccion) {
                    navigator.notification.alert(
                            'Ocurrió un error volver a intentarlo mas tarde.', // message
                            alertDismissed, // callback
                            '', // title
                            'Aceptar'                  // buttonName
                            );
                } else {
                    alert('Ocurrió un error volver a intentarlo mas tarde.');
                }
                $('#txt_edit_cliente_correo').val('');
            }
        }
    });
});

function validarEmail(valor) {
    var cadena = /^([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)@([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)[\\.]([a-zA-Z]{2,9})$/;
    if (cadena.test(valor)) {
        return true;
    } else {
        return false;
    }
}